/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef OBJECT_POOL_H
#define OBJECT_POOL_H
#include<list>
template<typename Object>
class ObjectPool
{
public:
    ObjectPool(size_t unSize):m_unSize(unSize)
    {
        for(size_t unIdx=0;unIdx<m_unSize;++unIdx)
        {
            m_oPool.push_back(new Object());
        }
    }
    ~ObjectPool()
    {
        typename std::list<Object*>::iterator iter=m_oPool.begin();
        while(iter!=m_oPool.end())
        {
            delete(*iter);
            ++iter;
        }
        m_unSize=0;
    }
    Object* GetObject()
    {
        Object* pObj=NULL;
        if(0==m_unSize)
        {
            pObj=new Object();
        }
        else
        {
            pObj=m_oPool.front();
            m_oPool.pop_front();
            --m_unSize;
        }
        return pObj;
    }
    void returnObject(Object* pObj)
    {
        m_oPool.push_back(pObj);
        ++m_unSize;
    }
private:
    size_t m_unSize;
    std::list<Object*>m_oPool;
};
#endif // OBJECT_POOL_H
