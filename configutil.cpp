/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "configutil.h"
#include <QtCore/QtCore>
#include <QDebug>

ConfigUtil::ConfigUtil(QString qstrfilename, QObject *parent) : QObject(parent),m_qstrFileName(qstrfilename)
{
    if (qstrfilename.isEmpty()){
//        m_qstrFileName = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +"/.config/ukui-clock/clock_conf.ini";
        m_qstrFileName = ":/clock_conf.ini";
    }else{
        m_qstrFileName = qstrfilename;
    }
    m_psetting = new QSettings(m_qstrFileName, QSettings::IniFormat);
    qDebug() << m_qstrFileName;
}

void ConfigUtil::Set(QString qstrnodename,QString qstrkeyname,QVariant qvarvalue)
{
    m_psetting->setValue(QString("/%1/%2").arg(qstrnodename).arg(qstrkeyname), qvarvalue);
}

QVariant ConfigUtil::Get(QString qstrnodename,QString qstrkeyname)
{
    QVariant qvar = m_psetting->value(QString("/%1/%2").arg(qstrnodename).arg(qstrkeyname));
    return qvar;
}
