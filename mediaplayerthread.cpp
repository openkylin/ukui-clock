/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "mediaplayerthread.h"
#include <QTime>



MediaPlayerThread::MediaPlayerThread(int size, QObject *parent):QThread(parent),m_size(size)
{

}

void MediaPlayerThread::run()
{
    qDebug()<<"dbq-开始创建对象池"<<m_size;
    qint64 timeT1 = QDateTime::currentDateTime().toMSecsSinceEpoch();
    ObjectPool<QMediaPlayer> * que = new ObjectPool<QMediaPlayer>(m_size);
    qint64 timeT2 = QDateTime::currentDateTime().toMSecsSinceEpoch();
    qDebug()<<"dbq-创建耗时"<<timeT2-timeT1;
    emit resultReady(que);
}
