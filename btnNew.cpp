/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "btnNew.h"
#include <QVBoxLayout>
#include <QDebug>
#include "clock.h"
#include <QPainterPath>
#include "theme.h"


Btn_new::Btn_new(int num, QString name, Btn_new::BTN_NEW_TYPE btnType, QWidget *parent) :
    QPushButton(parent),
    clock_num(num),
    m_btnType(btnType)
{
    QPixmap pixmap = QPixmap(":/image/go-bottom-symbolic.png");
    nameLabel = new QLabel(this);
    textLabel = new QLabel(this);
    IconLabel = new QLabel(this);
    noName = new QLabel(this);
    //num不同，name与text的大小配比不同
    int lineHeight = 23;
    int lineMoveHeight = 13;
    int nameWidth = 100;
    int editWidth = 200;
    nameLabel->setFixedSize(nameWidth-num, lineHeight);
    textLabel->setFixedSize(editWidth+num, lineHeight);
    IconLabel->setFixedSize(27, 36);
    noName->setFixedSize(9, 36);
    nameLabel->move(20, lineMoveHeight);
    textLabel->move(nameWidth-num, lineMoveHeight);
    noName->move(244, 0);
    IconLabel->move(309, 6);
    nameLabel->setText(name);
    textLabel->setText(name);
    IconLabel->setPixmap(pixmap);
    textLabel->setAlignment(Qt::AlignRight | Qt::AlignCenter);
    nameLabel->setStyleSheet("font-size:14px;");
    textLabel->setStyleSheet("font-size:14px;");
    this->resize(340,48);

    QPalette palette;
    palette.setColor(QPalette::ButtonText,QColor(148, 148, 148, 255));
    textLabel->setPalette(palette);
    clockNameLineEdit = new QLineEdit(this);
    clockNameLineEdit->move(nameWidth-num, 0);
    clockNameLineEdit->setFixedSize(editWidth+num, 48);
    clockNameLineEdit->setContextMenuPolicy(Qt::NoContextMenu);
    clockNameLineEdit->setAlignment(Qt::AlignRight);
    if(m_btnType==SELECT_BTN){
        textLabel->show();
        IconLabel->show();
        clockNameLineEdit->hide();
    }else if(m_btnType==LINE_EDIT){
        textLabel->hide();
        IconLabel->hide();
        clockNameLineEdit->show();
    }

}



Btn_new::~Btn_new()
{

}

void Btn_new::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    QPainterPath rectPath;
    rectPath.addRoundedRect(this->rect(), 10, 10); // 左上右下

    QPainter painter(this);
    QStyleOption opt;
    opt.init(this);
    painter.setBrush(opt.palette.color(QPalette::Base));

    QColor mainColor;
    mainColor = theme::selectBtnBackColor;
    p.fillPath(rectPath,QBrush(mainColor));
}
void Btn_new::updateWidthForFontChange(int px)
{
    //调整一下，不然放大字体会遮挡
    int wideth =nameLabel->size().width();
    nameLabel->setFixedWidth(wideth+px);
}
/**
 * @brief 1 向上 0 向下
 * @param status
 */
void Btn_new::updateIconLabel(int status)
{
     QPixmap pixmap ;
     if(status==1){
         pixmap = QPixmap(":/image/go-up-symbolic.png");
     }else{
         pixmap = QPixmap(":/image/go-bottom-symbolic.png");
     }
     IconLabel->setPixmap(pixmap);
     IconLabel->update();
}
