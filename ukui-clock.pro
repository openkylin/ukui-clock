#-------------------------------------------------
#
# Project created by QtCreator 2014-06-28T21:00:26
#
#-------------------------------------------------

QT       += core gui multimedia sql

# 适配窗口管理器圆角阴影
QT += KWindowSystem dbus x11extras

# 适配窗口管理器圆角阴影
LIBS +=-lpthread
LIBS +=-lX11
LIBS += -lukui-log4qt
# 配置gsettings
CONFIG += link_pkgconfig
PKGCONFIG += gsettings-qt
PKGCONFIG += kysdk-waylandhelper
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ukui-clock
TEMPLATE = app


CONFIG += c++11 link_pkgconfig
PKGCONFIG += gsettings-qt kysdk-qtwidgets

INCLUDEPATH += /usr/include/mpv/
LIBS += -lmpv
# 解析音频文件
LIBS += -lavformat

SOURCES += \
    CustomButton.cpp \
    about.cpp \
    adaptscreeninfo.cpp \
    baseverticalscroll.cpp \
    btnNew.cpp \
    clickableLabel.cpp \
    clock.cpp \
    clockentitydao.cpp \
    clockdbusadaptor.cpp \
    closeOrHide.cpp \
    configutil.cpp \
    commontooltip.cpp \
    coreplayer/mmediaplayer.cpp \
    coreplayer/mmediaplaylist.cpp \
    coreplayer/playcontroller.cpp \
    countdownAnimation.cpp \
    dbusbaseclockadaptor.cpp \
    customcombox.cpp \
    customlistwidget.cpp \
    dbusdeleteclockbyidadaptor.cpp \
    dbusnoticeshowagainadaptor.cpp \
    dbusselectclockbyidadaptor.cpp \
    dbusupdateclockadaptor.cpp \
    deleteMsg.cpp \
    dotlineDemo.cpp \
    fieldvalidutil.cpp \
    gsettingsubject.cpp \
    integer.cpp \
    itemNew.cpp \
    kylinqfiledialog.cpp \
    main.cpp \
    mediaplayerpool.cpp \
    mediaplayerthread.cpp \
    noticeAlarm.cpp \
    noticealarmpoolutil.cpp \
    primarymanager.cpp \
    qroundProgressBar.cpp \
    roundbtn.cpp \
    selectbtn.cpp \
    selectbtnutil.cpp \
    setAlarmRepeatDialog.cpp \
    singleApplication.cpp \
    stopwatchItem.cpp \
    theme.cpp \
    utils.cpp \
    verticalScroll24.cpp \
    verticalScroll60.cpp \
    verticalScroll99.cpp \
    tinycountdown.cpp \
    CJsonObject.cpp \
    cJSON.c \
    verticalscrollapm.cpp \
    xatom-helper.cpp

TRANSLATIONS += translations/ukui-clock_tr.ts \
                translations/ukui-clock_bo_CN.ts \
                translations/ukui-clock_de.ts \
                translations/ukui-clock_es.ts \
                translations/ukui-clock_fr.ts \
                translations/ukui-clock_ky.ts \
                translations/ukui-clock_kk.ts \
                translations/ukui-clock_mn.ts \
                translations/ukui-clock_ug.ts \
                translations/ukui-clock_zh_CN.ts \
                translations/ukui-clock_zh_HK.ts



HEADERS  += clock.h \
    ClockInterface.h \
    CustomButton.h \
    about.h \
    adaptscreeninfo.h \
    baseverticalscroll.h \
    btnNew.h \
    clickableLabel.h \
    clockentitydao.h \
    clockdbusadaptor.h \
    closeOrHide.h \
    configutil.h \
    commontooltip.h \
    connection.h \
    constant_class.h \
    coreplayer/mmediaplayer.h \
    coreplayer/mmediaplaylist.h \
    coreplayer/playcontroller.h \
    countdownAnimation.h \
    dbusbaseclockadaptor.h \
    customcombox.h \
    customlistwidget.h \
    dbusdeleteclockbyidadaptor.h \
    dbusnoticeshowagainadaptor.h \
    dbusselectclockbyidadaptor.h \
    dbusupdateclockadaptor.h \
    debug.h \
    deleteMsg.h \
    dotlineDemo.h \
    fieldvalidutil.h \
    gsettingsubject.h \
    integer.h \
    itemNew.h \
    kylinqfiledialog.h \
    mediaplayerpool.h \
    mediaplayerthread.h \
    noticeAlarm.h \
    noticealarmpoolutil.h \
    object_pool.h \
    primarymanager.h \
    qroundProgressBar.h \
    roundbtn.h \
    selectbtn.h \
    selectbtnutil.h \
    setAlarmRepeatDialog.h \
    singleApplication.h \
    stopwatchItem.h \
    theme.h \
    utils.h \
    verticalScroll24.h \
    verticalScroll60.h \
    verticalScroll99.h \
    tinycountdown.h \
    CJsonObject.hpp \
    cJSON.h \
    verticalscrollapm.h \
    xatom-helper.h


FORMS    += clock.ui \
    about.ui \
    closeOrHide.ui \
    deleteMsg.ui \
    noticeAlarm.ui \
    tinycountdown.ui


RESOURCES += \
    images.qrc

RC_FILE = clock.rc

unix {
    target.path = /usr/bin/
    INSTALLS += target

    music.path = /usr/share/ukui-clock/
    music.files += music/bark.wav
    music.files += music/drip.wav
    music.files += music/glass.wav
    music.files += music/sonar.wav
    INSTALLS += music

    translation.path = /usr/share/ukui-clock/ukui31
    translation.files += translations/*.qm
    INSTALLS += translation

    guide.path =  /usr/share/kylin-user-guide/data/guide/kylin-alarm-clock/
    guide.files += guide/*
    INSTALLS += guide


}
desktopfile.files = ukui-clock.desktop
desktopfile.path = /usr/share/applications/

INSTALLS += desktopfile

DISTFILES += \
    clock_conf.ini \
    image/DFPKingGothicGB-Semibold-2.ttf \
    image/noClockBlack.svg \
    music/bark.wav \
    music/drip.wav \
    music/glass.wav \
    music/sonar.wav
