/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "closeOrHide.h"
#include "utils.h"
//#include "ui_closeOrHide.h"
#include "QDebug"
#include <X11/Xlib.h>
#include "xatom-helper.h"
#include "constant_class.h"
#include "gsettingsubject.h"
#include <math.h>

extern void qt_blurImage(QImage &blurImage, qreal radius, bool quality, int transposed);

close_or_hide::close_or_hide(QWidget *parent) :
    QDialog(parent)
{
//    ui->setupUi(this);
    setupUi(this);
    iniframe();
//    ui->surebtn->setText("");
//    ui->cancelbtn->setText("");
//    this->setProperty("blurRegion", QRegion(QRect(1, 1, 1, 1)));
//    setAttribute(Qt::WA_TranslucentBackground);
//    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);

    backrunRadio->setChecked(1);

    // 添加窗管协议
//    XAtomHelper::setStandardWindowHint(this->winId());
//    XAtomHelper::setStandardWindowRadius(this->winId(),WINDOWN_RADIUS);
    kdk::UkuiStyleHelper::self()->removeHeader(this);
    closeInfoLabel->setText(tr("Please select the state after closing:"));
    closeInfoLabel->setWordWrap(true);
    closeInfoLabel->setAlignment(Qt::AlignTop);
    //调色板
//    QPalette palette = surebtn->palette();
//    palette.setColor(QPalette::Button,QColor(61,107,229,255));
//    palette.setBrush(QPalette::ButtonText, QBrush(Qt::white));
    //保留按钮
//    surebtn->setPalette(palette);
    //退出按钮
    QPalette palette1 = closebtn->palette();
    QColor ColorPlaceholderText1(255,255,255,0);
    QBrush brush;
    brush.setColor(ColorPlaceholderText1);
//    palette.setBrush(QPalette::Button, brush);
    closebtn->setPalette(palette1);
    closebtn->setIcon(QIcon::fromTheme("window-close-symbolic"));
    closebtn->setToolTip(tr("Close"));
    closebtn->setProperty("isWindowButton", 0x2);
    closebtn->setProperty("useIconHighlightEffect", 0x8);
    closebtn->setFlat(true);

//    主题框架1.0.6-5kylin2

    //配置重要按钮
    surebtn->setProperty("isImportant", true);
    cancelbtn->setProperty("useButtonPalette", true);
    //关闭按钮去掉聚焦状态
    closebtn->setFocusPolicy(Qt::NoFocus);

    //按钮文本居中显示，避免使用stylesheet，添加label于pushbutton上
    QLabel *sureLabel = new QLabel();
    sureLabel->setText(Utils::getOmitStr(tr("sure"),7));
    sureLabel->adjustSize();
    QHBoxLayout *myLayout = new QHBoxLayout(surebtn);
    myLayout->addWidget(sureLabel);
    myLayout->setContentsMargins(0,2,0,4);
    myLayout->setAlignment(Qt::AlignHCenter);
    surebtn->setLayout(myLayout);
    if(sureLabel->text().contains("...")){
        surebtn->setToolTip(tr("sure"));
    }

    QLabel *cancelLabel = new QLabel();
    cancelLabel->setText(Utils::getOmitStr(tr("cancel"),7));
    cancelLabel->adjustSize();
    QHBoxLayout *myLayout1 = new QHBoxLayout(cancelbtn);
    myLayout1->addWidget(cancelLabel);
    myLayout1->setContentsMargins(0,2,0,4);
    myLayout1->setAlignment(Qt::AlignHCenter);
    cancelbtn->setLayout(myLayout1);
    if(cancelLabel->text().contains("...")){
        cancelbtn->setToolTip(tr("cancel"));
    }

    questionIconLabel->setPixmap(QIcon::fromTheme("dialog-question").pixmap(24,24));

    connect(closebtn,SIGNAL(clicked()),this,SLOT(on_closebtn_clicked()));
    connect(surebtn,SIGNAL(clicked()),this,SLOT(on_surebtn_clicked()));
    connect(cancelbtn,SIGNAL(clicked()),this,SLOT(on_cancelbtn_clicked()));
    settingsStyle();
    bool onTablet = GsettingSubject::getInstance()->getOnTablet();
    updatePosition(onTablet);
}

close_or_hide::~close_or_hide()
{
//    delete ui;
}

void close_or_hide::setupUi(QDialog *close_or_hide)
{
    if (close_or_hide->objectName().isEmpty())
        close_or_hide->setObjectName(QString::fromUtf8("close_or_hide"));
    close_or_hide->resize(350, 174);
    retranslateUi(close_or_hide);

    QMetaObject::connectSlotsByName(close_or_hide);
} // setupUi

void close_or_hide::retranslateUi(QDialog *close_or_hide)
{
    close_or_hide->setWindowTitle(QApplication::translate("close_or_hide", "Dialog", nullptr));
} // retranslateUi

void close_or_hide::iniframe()
{

    widget = new QWidget(this);
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
    widget->setSizePolicy(sizePolicy);

    verticalLayoutWidget = new QWidget(widget);

    horizontalLayout = new QHBoxLayout(verticalLayoutWidget);

    closebtn = new QPushButton(verticalLayoutWidget);

    horizontalLayout->addWidget(closebtn);

    verticalLayoutWidget_2 = new QWidget(widget);

    horizontalLayout_2 = new QHBoxLayout(verticalLayoutWidget_2);
    horizontalLayout_2->setSpacing(0);
    horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
    horizontalSpacer = new QSpacerItem(57, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer);

    backrunRadio = new QRadioButton(verticalLayoutWidget_2);
    QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(backrunRadio->sizePolicy().hasHeightForWidth());
    backrunRadio->setSizePolicy(sizePolicy1);
    QFont font;
    font.setPointSize(11);
    backrunRadio->setFont(font);
    backrunRadio->setText(Utils::getOmitStr(tr(" backstage"),15));
    if(backrunRadio->text().contains("...")){
        backrunRadio->setToolTip(tr(" backstage"));
    }



    horizontalLayout_2->addWidget(backrunRadio);

    horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer_3);

    exitRadio = new QRadioButton(verticalLayoutWidget_2);
    sizePolicy1.setHeightForWidth(exitRadio->sizePolicy().hasHeightForWidth());
    exitRadio->setSizePolicy(sizePolicy1);
    exitRadio->setMinimumSize(QSize(120, 0));
    exitRadio->setMaximumSize(QSize(120, 16777215));
    exitRadio->setFont(font);
    exitRadio->setText(Utils::getOmitStr(tr(" Exit program"),11));
    if(exitRadio->text().contains("...")){
        exitRadio->setToolTip(tr(" Exit program"));
    }


    horizontalLayout_2->addWidget(exitRadio);

    horizontalSpacer_2 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer_2);

    widget_2 = new QWidget(widget);
    QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
    widget_2->setSizePolicy(sizePolicy2);
    horizontalLayout_3 = new QHBoxLayout(widget_2);
    horizontalLayout_3->setSpacing(0);
    horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
    horizontalSpacer_4 = new QSpacerItem(156, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer_4);

    cancelbtn = new QPushButton(widget_2);
    cancelbtn->setFont(font);

    horizontalLayout_3->addWidget(cancelbtn);

    horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer_5);

    surebtn = new QPushButton(widget_2);
    surebtn->setFont(font);

    horizontalLayout_3->addWidget(surebtn);

    horizontalSpacer_6 = new QSpacerItem(24, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer_6);

    widget_3 = new QWidget(widget);

    horizontalLayout_4 = new QHBoxLayout(widget_3);
    horizontalLayout_4->setSpacing(0);
    horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
    horizontalSpacer_7 = new QSpacerItem(24, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

    horizontalLayout_4->addItem(horizontalSpacer_7);

    questionIconLabel = new QLabel(widget_3);
    questionIconLabel->setFixedSize(24,24);

    horizontalLayout_4->addWidget(questionIconLabel, 0, Qt::AlignVCenter);

    closeInfoLabel = new QLabel(widget_3);
    closeInfoLabel->setFont(font);
    closeInfoLabel->setMargin(0);

    horizontalLayout_4->addWidget(closeInfoLabel, 0, Qt::AlignVCenter);
}

void close_or_hide::updatePosition(bool onTablet)
{
    if(onTablet){
        this->setFixedSize(350,214);
        widget->setGeometry(QRect(0, 0, 350, 214));
        verticalLayoutWidget->setGeometry(QRect(290, 0, 61, 57));
        horizontalLayout->setContentsMargins(5, 8, 8, 0);
        closebtn->setFixedSize(48,48);
        verticalLayoutWidget_2->setGeometry(QRect(0, 90, 351, 40));
        backrunRadio->setIconSize(QSize(16, 16));
        exitRadio->setIconSize(QSize(16, 16));
        widget_2->setGeometry(QRect(0, 138, 351, 60));
        horizontalSpacer_4->changeSize(150,20);
        cancelbtn->setFixedSize(80,48);
        surebtn->setFixedSize(80,48);
        widget_3->setGeometry(QRect(0, 50, 351, 41));
    }else{
        this->setFixedSize(350,174);
        widget->setGeometry(QRect(0, 0, 350, 174));
        verticalLayoutWidget->setGeometry(QRect(310, 0, 40, 36));
        horizontalLayout->setContentsMargins(5, 5, 5, 0);
        closebtn->setFixedSize(30,30);
        verticalLayoutWidget_2->setGeometry(QRect(0, 60, 351, 40));
        backrunRadio->setIconSize(QSize(14, 14));
        exitRadio->setIconSize(QSize(14, 14));
        widget_2->setGeometry(QRect(0, 120, 351, 31));
        horizontalSpacer_4->changeSize(156,20);
        cancelbtn->setFixedSize(80,30);
        surebtn->setFixedSize(80,30);
        widget_3->setGeometry(QRect(0, 20, 351, 41));
    }
}

void close_or_hide::on_closebtn_clicked()
{
    this->hide();
    close_flag = 0;
}

void close_or_hide::on_surebtn_clicked()
{
    if(backrunRadio->isChecked()==true){
        this->hide();
         close_flag = 1;
    }else{
        this->hide();
        close_flag = 2;
    }
}

void close_or_hide::on_cancelbtn_clicked()
{
    this->hide();
}

void close_or_hide::settingsStyle()
{
    GsettingSubject * subject = GsettingSubject::getInstance();
    connect(subject,&GsettingSubject::iconChnaged, this,[=](){
        questionIconLabel->setPixmap(QIcon::fromTheme("dialog-question").pixmap(24,24));
    });
    connect(subject,&GsettingSubject::tabletModeChange, this,[=](bool currentTablet){
        updatePosition(currentTablet);
    });
    subject->iniTabletMode();
}

void close_or_hide::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    QPainterPath rectPath;
    rectPath.addRect(this->rect());
    p.fillPath(rectPath,palette().color(QPalette::Base));
}

void close_or_hide::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        this->dragPosition = event->globalPos() - frameGeometry().topLeft();
        this->mousePressed = true;
    }
    QWidget::mousePressEvent(event);
}

void close_or_hide::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        this->mousePressed = false;
        this->setCursor(Qt::ArrowCursor);
    }

    QWidget::mouseReleaseEvent(event);
}

void close_or_hide::mouseMoveEvent(QMouseEvent *event)
{
    if (this->mousePressed) {
        move(event->globalPos() - this->dragPosition);
        this->setCursor(Qt::ClosedHandCursor);
    }

    QWidget::mouseMoveEvent(event);
}
