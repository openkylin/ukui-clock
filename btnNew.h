/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BTN_NEW_H
#define BTN_NEW_H

#include <QWidget>
#include <QToolButton>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

namespace Ui {
class Btn_new;
}
class Clock;
class Btn_new : public  QPushButton
{
    Q_OBJECT

public:
    enum BTN_NEW_TYPE{
        LINE_EDIT=0,
        SELECT_BTN=1
    };
    explicit Btn_new(int num, QString name ,BTN_NEW_TYPE btnType= SELECT_BTN,QWidget *parent = nullptr);
    ~Btn_new();
    QLabel *nameLabel;
    QLabel *textLabel;
    QLabel *noName;
    QLabel *IconLabel;
    QLineEdit * clockNameLineEdit;


    void paintEvent(QPaintEvent *event);
    void updateWidthForFontChange(int px);
    void updateIconLabel(int status);
private:
    Ui::Btn_new *ui;
    Clock * m_pclock;
    int clock_num;
    int pressflag;
    BTN_NEW_TYPE m_btnType;


protected:
};

#endif // BTN_NEW_H
