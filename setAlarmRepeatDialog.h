/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SET_ALARM_REPEAT_DIALOG_H
#define SET_ALARM_REPEAT_DIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QLabel>
#include <QPainter>
#include <QGSettings>
#include "clickableLabel.h"
#include "constant_class.h"
#include "gsettingsubject.h"



namespace Ui {
class set_alarm_repeat_Dialog;
}

class set_alarm_repeat_widget;


class set_alarm_repeat_Dialog : public QWidget
{
    Q_OBJECT

public:
    explicit set_alarm_repeat_Dialog(int width, int Length, int rowNum = 0, QWidget *parent = nullptr);
    ~set_alarm_repeat_Dialog();

    void paintEvent(QPaintEvent *event) override;
    set_alarm_repeat_widget *widget[20];
    QListWidgetItem *aItem[20];
    int rowNum_all ;
    int width_num, Length_num;
    QListWidget *listWidget;
    void  set_aItem(int rowNum,int flags);
    void setLabelHoverStyle(int num);
protected:
    void closeEvent(QCloseEvent *event) override;

signals:
     void dialogClose();

private:
    void  setupUi(QWidget *set_alarm_repeat_Dialog);
    void  retranslateUi(QWidget *set_alarm_repeat_Dialog);
    void  settingsStyle();                                                               //监听主题
    void  blackStyle();                                                                  //黑色主题
    void  whiteStyle();                                                                  //白色主题
    void updateLabelFront(QLabel *label, int size);
    void updateSelectItemLabelFont(int size);
    int CURRENT_FONT_SIZE;
};



class set_alarm_repeat_widget : public QWidget
{
    Q_OBJECT

public:
    explicit set_alarm_repeat_widget(QWidget *parent = nullptr);
    ~set_alarm_repeat_widget();

//    void paintEvent(QPaintEvent *event);

    QLabel *alarmLabel0;
    //QPushButton
    QLabel *alarmLabel1;

private:

};

#endif // SET_ALARM_REPEAT_DIALOG_H
