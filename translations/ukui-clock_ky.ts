<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>Dialog</source>
        <translation>دىيالوگ كەرەنەسى</translation>
    </message>
    <message>
        <location filename="../about.ui" line="87"/>
        <location filename="../about.ui" line="162"/>
        <source>Alarm</source>
        <translation>قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <source>Kylin Alarm</source>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Kylin Clock</source>
        <translatorcomment>麒麟 闹钟</translatorcomment>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>جۅنۉندۅ</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Version: </source>
        <translatorcomment>版本：</translatorcomment>
        <translation>باسماسى : </translation>
    </message>
    <message>
        <source>Version: 2020.1.0</source>
        <translatorcomment>版本： 2020.1.22</translatorcomment>
        <translation type="vanished">版本： 2020.1.22</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="121"/>
        <location filename="../about.cpp" line="129"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>تەيلۅۅ &gt; قولدوش ، دەم بەرىش : </translation>
    </message>
    <message>
        <source>Support and service team: support@kylinos.cn</source>
        <translatorcomment>服务与支持团队： support@kylinos.cn</translatorcomment>
        <translation type="vanished">服务与支持团队： support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>BaseVerticalScroll</name>
    <message>
        <location filename="../baseverticalscroll.cpp" line="103"/>
        <source>AM</source>
        <translation>تۉشتۅن مۇرۇن</translation>
    </message>
    <message>
        <location filename="../baseverticalscroll.cpp" line="105"/>
        <source>PM</source>
        <translation>تۉشتۅن كىيىن</translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="491"/>
        <location filename="../clock.cpp" line="498"/>
        <location filename="../clock.cpp" line="520"/>
        <location filename="../clock.cpp" line="1509"/>
        <location filename="../clock.cpp" line="1615"/>
        <location filename="../clock.cpp" line="3570"/>
        <source>start</source>
        <translatorcomment>开始</translatorcomment>
        <translation>باشتوو</translation>
    </message>
    <message>
        <source>5min</source>
        <translatorcomment>5分钟</translatorcomment>
        <translation type="vanished">5分钟</translation>
    </message>
    <message>
        <source>15min</source>
        <translatorcomment>15分钟</translatorcomment>
        <translation type="vanished">15分钟</translation>
    </message>
    <message>
        <source>25min</source>
        <translatorcomment>25分钟</translatorcomment>
        <translation type="vanished">25分钟</translation>
    </message>
    <message>
        <source>30min</source>
        <translation type="vanished">30分钟</translation>
    </message>
    <message>
        <source>60min</source>
        <translation type="vanished">60分钟</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="499"/>
        <location filename="../clock.cpp" line="1348"/>
        <location filename="../clock.cpp" line="3575"/>
        <location filename="../clock.cpp" line="3835"/>
        <source>suspend</source>
        <translation>ۇباقتىلۇۇ توقتوتۇۇ</translation>
    </message>
    <message>
        <source>tiny window</source>
        <translation type="vanished">迷你窗口</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="506"/>
        <source>add</source>
        <translatorcomment>添加</translatorcomment>
        <translation>قوشۇۇ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="512"/>
        <source>no alarm</source>
        <translatorcomment>无闹钟</translatorcomment>
        <translation>قوڭعۇراقتۇۇ ساات  جوق</translation>
    </message>
    <message>
        <source>delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="527"/>
        <source>save</source>
        <translatorcomment>保存</translatorcomment>
        <translation>ساقتوو</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">提醒铃声</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2453"/>
        <location filename="../clock.cpp" line="3706"/>
        <location filename="../clock.cpp" line="3809"/>
        <source>PM</source>
        <translation>تۉشتۅن كىيىن</translation>
    </message>
    <message>
        <source>add alarm</source>
        <translatorcomment>添加</translatorcomment>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Remaining time</source>
        <translation type="vanished">点击闹钟显示剩余时间</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1393"/>
        <source>reset</source>
        <translation>العاچىنا كەتىرىش</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="519"/>
        <location filename="../clock.cpp" line="1370"/>
        <location filename="../clock.cpp" line="1542"/>
        <source>count</source>
        <translation>ىرەت  ەسەپتۅۅ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="200"/>
        <location filename="../clock.cpp" line="3396"/>
        <location filename="../clock.cpp" line="3478"/>
        <source>Count down</source>
        <translation>تەسكەرى  ساناق</translation>
    </message>
    <message>
        <source>clock</source>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="vanished">秒表</translation>
    </message>
    <message>
        <source>deletealarm</source>
        <translation type="vanished">删除闹铃</translation>
    </message>
    <message>
        <source>Preservation</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>12hour43minThe bell rings</source>
        <translation type="vanished">12小时43分后铃声响</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="523"/>
        <location filename="../clock.cpp" line="525"/>
        <source>cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
    <message>
        <source>New alarm</source>
        <translation type="vanished">新建闹钟</translation>
    </message>
    <message>
        <source>  Name</source>
        <translation type="vanished">  闹钟名</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2159"/>
        <source>  repeat</source>
        <translation>  قايتالوو</translation>
    </message>
    <message>
        <source>  Remind</source>
        <translation type="vanished">  铃声</translation>
    </message>
    <message>
        <source>  ring time</source>
        <translation type="vanished">  铃声时长</translation>
    </message>
    <message>
        <source> ring time</source>
        <translation type="vanished"> 铃声时长</translation>
    </message>
    <message>
        <source>开始</source>
        <translatorcomment>start</translatorcomment>
        <translation type="vanished">start</translation>
    </message>
    <message>
        <source>5分钟</source>
        <translatorcomment>5min</translatorcomment>
        <translation type="vanished">5min</translation>
    </message>
    <message>
        <source>10分钟</source>
        <translatorcomment>10min</translatorcomment>
        <translation type="vanished">10min</translation>
    </message>
    <message>
        <source>20分钟</source>
        <translatorcomment>20min</translatorcomment>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>30分钟</source>
        <translatorcomment>30min</translatorcomment>
        <translation type="vanished">30min</translation>
    </message>
    <message>
        <source>60分钟</source>
        <translatorcomment>60min</translatorcomment>
        <translation type="vanished">60min</translation>
    </message>
    <message>
        <source>下午05:31</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>暂停</source>
        <translatorcomment>suspend</translatorcomment>
        <translation type="vanished">suspend</translation>
    </message>
    <message>
        <source>  提醒铃声</source>
        <translatorcomment>Remind</translatorcomment>
        <translation type="vanished"> Remind</translation>
    </message>
    <message>
        <source>添加闹钟</source>
        <translatorcomment>add alarm</translatorcomment>
        <translation type="vanished">add alarm</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1543"/>
        <source>interval </source>
        <translation>ارالىق </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2069"/>
        <source>Version：</source>
        <translation>باسماسى :</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2351"/>
        <source>recent alarm</source>
        <translation>جاقىندا قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="528"/>
        <location filename="../clock.cpp" line="2460"/>
        <location filename="../clock.cpp" line="3708"/>
        <location filename="../clock.cpp" line="3816"/>
        <source>AM</source>
        <translation>تۉشتۅن مۇرۇن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2047"/>
        <source>Option</source>
        <translation>تاندالما</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2495"/>
        <location filename="../clock.cpp" line="2981"/>
        <location filename="../clock.cpp" line="3237"/>
        <location filename="../clock.cpp" line="4251"/>
        <source>Mon</source>
        <translation>دۉيشۉنبۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2497"/>
        <location filename="../clock.cpp" line="2983"/>
        <location filename="../clock.cpp" line="3238"/>
        <location filename="../clock.cpp" line="4252"/>
        <source>Tue</source>
        <translation>سەيشەمبى</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2499"/>
        <location filename="../clock.cpp" line="2985"/>
        <location filename="../clock.cpp" line="3239"/>
        <location filename="../clock.cpp" line="4253"/>
        <source>Wed</source>
        <translation>چارشەمبى</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2501"/>
        <location filename="../clock.cpp" line="2987"/>
        <location filename="../clock.cpp" line="3240"/>
        <location filename="../clock.cpp" line="4254"/>
        <source>Thu</source>
        <translation>پەيشەمبى</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2503"/>
        <location filename="../clock.cpp" line="2989"/>
        <location filename="../clock.cpp" line="3241"/>
        <location filename="../clock.cpp" line="4255"/>
        <source>Fri</source>
        <translation>جۇما</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2505"/>
        <location filename="../clock.cpp" line="2991"/>
        <location filename="../clock.cpp" line="3242"/>
        <location filename="../clock.cpp" line="4256"/>
        <source>Sat</source>
        <translation>ىشەنبە</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2507"/>
        <location filename="../clock.cpp" line="2993"/>
        <location filename="../clock.cpp" line="3243"/>
        <location filename="../clock.cpp" line="4257"/>
        <source>Sun</source>
        <translation>جەكشەنبە</translation>
    </message>
    <message>
        <source>60 Seconds to close</source>
        <translation type="vanished">60秒后自动关闭</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3765"/>
        <location filename="../clock.cpp" line="4469"/>
        <source>five mins late</source>
        <translation>بەش مىنۇتتان كىيىن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3766"/>
        <location filename="../clock.cpp" line="4472"/>
        <source>ten mins late</source>
        <translation>ون مىنۇتتان كىيىن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3767"/>
        <location filename="../clock.cpp" line="4475"/>
        <source>twenty mins late</source>
        <translation>يىگىرمە مىنۇتتان كىيىن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3768"/>
        <location filename="../clock.cpp" line="4478"/>
        <source>thirsty mins late</source>
        <translation>ئوتتۇز مىنۇتتان كىيىن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3769"/>
        <location filename="../clock.cpp" line="4481"/>
        <source>one hour late</source>
        <translation>بىر  سائەتتىن كىيىن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4679"/>
        <source>mini window</source>
        <translation>كىچىك  كۅزۅنۅكتۅ كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2318"/>
        <source>2min</source>
        <translation>ەكى  مىنۇت</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="14"/>
        <source>Alarm</source>
        <translation>قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">计次</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="202"/>
        <source>Watch</source>
        <translation>سىكونت ساات</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="890"/>
        <location filename="../clock.cpp" line="893"/>
        <location filename="../clock.cpp" line="907"/>
        <location filename="../clock.cpp" line="908"/>
        <location filename="../clock.cpp" line="2162"/>
        <source>  bell</source>
        <translation>  قوڭعۇراق  وبونۇ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2025"/>
        <source>Minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation>كىچىرەيتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="565"/>
        <location filename="../clock.cpp" line="2061"/>
        <source>Quit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>جانىش ، قايتىش</translation>
    </message>
    <message>
        <source>Menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1053"/>
        <source>Delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation>ۅچۉرۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1054"/>
        <source>ClearAll</source>
        <translatorcomment>全部清空</translatorcomment>
        <translation>باردىعىن تازالوو</translation>
    </message>
    <message>
        <source>Set Up</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2059"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>جەرلىك</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2060"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>جۅنۉندۅ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2035"/>
        <source>Close</source>
        <translatorcomment>关闭</translatorcomment>
        <translation>بەكىتىش</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1568"/>
        <source>up to 100 times</source>
        <translation>ەڭ كۅپ بولعوندو  100 ىرەت</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2165"/>
        <source>  remind</source>
        <translation>  سەل  تۇرۇپ ەسكەرتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2156"/>
        <source>  name</source>
        <translation>  قوڭعۇراقتۇۇ سااتتىن  ناامى</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1055"/>
        <source>edit</source>
        <translation>تۅپتۅش</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2011"/>
        <source>mute</source>
        <translation>دووشسۇز</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1670"/>
        <source>All bells are off</source>
        <translation>باردىق قوڭعۇراق دووشتۇ   جابۇۇ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2320"/>
        <source>3min</source>
        <translation>3 مىنوت</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2322"/>
        <source>4min</source>
        <translation>4 مىنوت</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2324"/>
        <source>6min</source>
        <translation>6 مىنوت</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2331"/>
        <location filename="../clock.cpp" line="2522"/>
        <location filename="../clock.cpp" line="2899"/>
        <location filename="../clock.cpp" line="3007"/>
        <location filename="../clock.cpp" line="3008"/>
        <location filename="../clock.cpp" line="3235"/>
        <location filename="../clock.cpp" line="4262"/>
        <location filename="../clock.cpp" line="4263"/>
        <source>No repetition</source>
        <translation>تەكرارلانمىسۇن</translation>
    </message>
    <message>
        <source> Seconds to close</source>
        <translation type="vanished"> 秒后关闭</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2520"/>
        <location filename="../clock.cpp" line="2592"/>
        <location filename="../clock.cpp" line="3236"/>
        <location filename="../clock.cpp" line="4276"/>
        <location filename="../clock.cpp" line="4277"/>
        <source>Workingday</source>
        <translation>قىزمات كۉنۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2605"/>
        <source>(default)</source>
        <translation>(الدىن  بەكىتىلگەن )</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2614"/>
        <location filename="../clock.cpp" line="3764"/>
        <location filename="../clock.cpp" line="4462"/>
        <location filename="../clock.cpp" line="4466"/>
        <location filename="../clock.cpp" line="4484"/>
        <source>none</source>
        <translation>جوق</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2677"/>
        <location filename="../clock.cpp" line="2723"/>
        <source>Please set alarm name!</source>
        <translatorcomment>请设置闹钟名!</translatorcomment>
        <translation>!قوڭعۇراقتۇۇ ساات  اتاعىن بەكىتىڭ</translation>
    </message>
    <message>
        <source>hour </source>
        <translation type="vanished">小时 </translation>
    </message>
    <message>
        <source> min bell rings</source>
        <translation type="vanished"> 分钟后铃响</translation>
    </message>
    <message>
        <source>Edit alarm clock</source>
        <translation type="vanished">编辑闹钟</translation>
    </message>
    <message>
        <source>点击闹钟显示剩余时间</source>
        <translatorcomment>Remaining time</translatorcomment>
        <translation type="vanished">Remaining time</translation>
    </message>
    <message>
        <source> days </source>
        <translation type="vanished"> 天 </translation>
    </message>
    <message>
        <source> hour </source>
        <translation type="vanished"> 小时 </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2896"/>
        <source>glass</source>
        <translation>اينەك</translation>
    </message>
    <message>
        <source>minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>close</source>
        <translatorcomment>关闭</translatorcomment>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1381"/>
        <location filename="../clock.cpp" line="3855"/>
        <source>continue</source>
        <translation>جالعاشتىرىش</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>warning</source>
        <translation>ەسكەرتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>the number of alarms reaches limit!！</source>
        <translation>!قوڭعۇراقتۇۇ ساات  سانى  چەككە جەتى</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>yes</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3004"/>
        <location filename="../clock.cpp" line="3005"/>
        <source>  work</source>
        <translation>  قىزمات كۉنۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3004"/>
        <source>  工作日</source>
        <translation>  قىزمات كۉنۉ</translation>
    </message>
    <message>
        <source>360 Seconds to close</source>
        <translation type="vanished">360秒后关闭</translation>
    </message>
    <message>
        <source>Time out</source>
        <translation type="vanished">时间到</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3539"/>
        <source>End</source>
        <translation>سوڭۇنا چىعۇۇ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3691"/>
        <source>after tomorrow</source>
        <translation>ئۆگۈن</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3694"/>
        <source>Tomorrow</source>
        <translation>ئەتە</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4203"/>
        <source>hour</source>
        <translation>ساات</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4206"/>
        <source>min</source>
        <translation>ەڭ قىسقا</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3912"/>
        <source>sec</source>
        <translation>سىكونت</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2517"/>
        <location filename="../clock.cpp" line="2518"/>
        <location filename="../clock.cpp" line="3002"/>
        <location filename="../clock.cpp" line="4399"/>
        <source>Every day</source>
        <translation>ار بىر كۉن</translation>
    </message>
    <message>
        <source>glass(default)</source>
        <translation type="vanished">玻璃(默认)</translation>
    </message>
    <message>
        <source>bark(default)</source>
        <translation type="vanished">犬吠(默认)</translation>
    </message>
    <message>
        <source>sonar(default)</source>
        <translation type="vanished">声呐(默认)</translation>
    </message>
    <message>
        <source>drip(default)</source>
        <translation type="vanished">雨滴(默认)</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1分钟</translation>
    </message>
    <message>
        <source>Monday to Friday</source>
        <translation type="vanished">周一周二周三周四周五</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>复位</source>
        <translatorcomment>reset</translatorcomment>
        <translation type="vanished">reset</translation>
    </message>
    <message>
        <source>计次</source>
        <translatorcomment>count</translatorcomment>
        <translation type="vanished">count</translation>
    </message>
    <message>
        <source>删除闹铃</source>
        <translatorcomment>deletealarm</translatorcomment>
        <translation type="vanished">deletealarm</translation>
    </message>
    <message>
        <source>保存</source>
        <translatorcomment>Preservation</translatorcomment>
        <translation type="vanished">Preservation</translation>
    </message>
    <message>
        <source>倒计时</source>
        <translatorcomment>Count down</translatorcomment>
        <translation type="vanished">Count down</translation>
    </message>
    <message>
        <source>闹钟</source>
        <translatorcomment>Alarm</translatorcomment>
        <translation type="obsolete">Alarm</translation>
    </message>
    <message>
        <source>秒表</source>
        <translatorcomment>Stopwatch</translatorcomment>
        <translation type="vanished">Stopwatch</translation>
    </message>
    <message>
        <source>12小时43分后铃声响</source>
        <translatorcomment>12hour43minThe bell rings</translatorcomment>
        <translation type="vanished">12hour43minThe bell rings</translation>
    </message>
    <message>
        <source>取消</source>
        <translatorcomment>cancel</translatorcomment>
        <translation type="vanished">cancel</translation>
    </message>
    <message>
        <source>新建闹钟</source>
        <translatorcomment>New alarm</translatorcomment>
        <translation type="vanished">New alarm</translation>
    </message>
    <message>
        <source>  闹钟名</source>
        <translatorcomment> Name</translatorcomment>
        <translation type="vanished"> Name</translation>
    </message>
    <message>
        <source>  重复</source>
        <translatorcomment> repeat</translatorcomment>
        <translation type="vanished"> repeat</translation>
    </message>
    <message>
        <source>  铃声时长</source>
        <translatorcomment> ring time</translatorcomment>
        <translation type="vanished"> ring time</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="52"/>
        <source>On</source>
        <translation>اچىق</translation>
    </message>
    <message>
        <source>继续</source>
        <translatorcomment>Continue</translatorcomment>
        <translation type="vanished">Continue</translation>
    </message>
    <message>
        <source>间隔 </source>
        <translatorcomment>interval </translatorcomment>
        <translation type="obsolete">interval </translation>
    </message>
    <message>
        <source>下午</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>上午</source>
        <translatorcomment>AM</translatorcomment>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>周一</source>
        <translatorcomment>Mon</translatorcomment>
        <translation type="vanished">Mon</translation>
    </message>
    <message>
        <source>周二</source>
        <translatorcomment>Tue</translatorcomment>
        <translation type="vanished">Tue</translation>
    </message>
    <message>
        <source>周三</source>
        <translatorcomment>Wed</translatorcomment>
        <translation type="vanished">Wed</translation>
    </message>
    <message>
        <source>周四</source>
        <translatorcomment>Thu</translatorcomment>
        <translation type="vanished">Thu</translation>
    </message>
    <message>
        <source>周五</source>
        <translatorcomment>Fri</translatorcomment>
        <translation type="vanished">Fri</translation>
    </message>
    <message>
        <source>周六</source>
        <translatorcomment>Sat</translatorcomment>
        <translation type="vanished">Sat</translation>
    </message>
    <message>
        <source>周日</source>
        <translatorcomment>Sun</translatorcomment>
        <translation type="vanished">Sun</translation>
    </message>
    <message>
        <source>秒后自动关闭</source>
        <translatorcomment> Seconds to close</translatorcomment>
        <translation type="vanished"> Seconds to close</translation>
    </message>
    <message>
        <source>2分钟后提醒</source>
        <translation type="vanished">Alert in 2 minutes</translation>
    </message>
    <message>
        <source>5分钟后提醒</source>
        <translation type="vanished">Alert in 5 minutes</translation>
    </message>
    <message>
        <source>10分钟后提醒</source>
        <translation type="vanished">Alert in 10 minutes</translation>
    </message>
    <message>
        <source>30分钟后提醒</source>
        <translation type="vanished">Alert in 30 minutes</translation>
    </message>
    <message>
        <source>60分钟后提醒</source>
        <translation type="vanished">Alert in 60 minutes</translation>
    </message>
    <message>
        <source>天</source>
        <translation type="obsolete"> days </translation>
    </message>
    <message>
        <source>玻璃</source>
        <translatorcomment>glass</translatorcomment>
        <translation type="vanished">glass</translation>
    </message>
    <message>
        <source>犬吠</source>
        <translatorcomment>bark</translatorcomment>
        <translation type="vanished">bark</translation>
    </message>
    <message>
        <source>声呐</source>
        <translatorcomment>sonar</translatorcomment>
        <translation type="vanished">sonar</translation>
    </message>
    <message>
        <source>雨滴</source>
        <translatorcomment>drip</translatorcomment>
        <translation type="vanished">drip</translation>
    </message>
    <message>
        <source>2分钟</source>
        <translatorcomment>2min</translatorcomment>
        <translation type="vanished">2min</translation>
    </message>
    <message>
        <source>3分钟</source>
        <translatorcomment>3min</translatorcomment>
        <translation type="vanished">3min</translation>
    </message>
    <message>
        <source>4分钟</source>
        <translatorcomment>4min</translatorcomment>
        <translation type="vanished">4min</translation>
    </message>
    <message>
        <source>6分钟</source>
        <translatorcomment>6min</translatorcomment>
        <translation type="vanished">6min</translation>
    </message>
    <message>
        <source>工作日</source>
        <translatorcomment>Workingday</translatorcomment>
        <translation type="vanished">Workingday</translation>
    </message>
    <message>
        <source>(默认)</source>
        <translatorcomment>(default)</translatorcomment>
        <translation type="vanished">(default)</translation>
    </message>
    <message>
        <source>每天</source>
        <translatorcomment>Every day</translatorcomment>
        <translation type="vanished">Every day</translation>
    </message>
    <message>
        <source>1分钟</source>
        <translatorcomment>1min</translatorcomment>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>小时</source>
        <translatorcomment> hour </translatorcomment>
        <translation type="obsolete"> hour </translation>
    </message>
    <message>
        <source>分钟后铃响</source>
        <translatorcomment> min bell rings</translatorcomment>
        <translation type="obsolete"> min bell rings</translation>
    </message>
    <message>
        <source>编辑闹钟</source>
        <translatorcomment>Edit alarm clock</translatorcomment>
        <translation type="vanished">Edit alarm clock</translation>
    </message>
    <message>
        <source>删除当前闹钟！</source>
        <translatorcomment>delete alame clock !</translatorcomment>
        <translation type="vanished">delete alame clock !</translation>
    </message>
    <message>
        <source>您确定删除当前闹钟吗？</source>
        <translatorcomment>are you sure ?</translatorcomment>
        <translation type="vanished">are you sure ?</translation>
    </message>
    <message>
        <source>倒计时时间结束</source>
        <translatorcomment>End of countdown time</translatorcomment>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>结束</source>
        <translatorcomment>End</translatorcomment>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>明日</source>
        <translatorcomment>Tom</translatorcomment>
        <translation type="vanished">Tom</translation>
    </message>
    <message>
        <source>360秒后自动关闭</source>
        <translatorcomment>360 Seconds to close</translatorcomment>
        <translation type="obsolete">360 Seconds to close</translation>
    </message>
    <message>
        <source>时间到</source>
        <translation type="vanished">Time out</translation>
    </message>
    <message>
        <source>后天</source>
        <translatorcomment>after tomorrow</translatorcomment>
        <translation type="vanished">after tomorrow</translation>
    </message>
    <message>
        <source>明天</source>
        <translatorcomment>Tomorrow</translatorcomment>
        <translation type="vanished">Tomorrow</translation>
    </message>
    <message>
        <source>时</source>
        <translatorcomment>hour</translatorcomment>
        <translation type="vanished">hour</translation>
    </message>
    <message>
        <source>分</source>
        <translatorcomment>min</translatorcomment>
        <translation type="vanished">min</translation>
    </message>
    <message>
        <source>秒</source>
        <translatorcomment>sec</translatorcomment>
        <translation type="vanished">sec</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3007"/>
        <source>不重复</source>
        <translatorcomment>No repetition </translatorcomment>
        <translation>قايتىلانمايدۇ</translation>
    </message>
    <message>
        <source>玻璃(默认)</source>
        <translatorcomment>glass(default)</translatorcomment>
        <translation type="vanished">glass(default)</translation>
    </message>
    <message>
        <source>犬吠(默认)</source>
        <translatorcomment>bark(default)</translatorcomment>
        <translation type="obsolete">bark(default)</translation>
    </message>
    <message>
        <source>声呐(默认)</source>
        <translatorcomment>sonar(default)</translatorcomment>
        <translation type="obsolete">sonar(default)</translation>
    </message>
    <message>
        <source>雨滴(默认)</source>
        <translatorcomment>drip(default)</translatorcomment>
        <translation type="obsolete">drip(default)</translation>
    </message>
    <message>
        <source>1分钟(默认)</source>
        <translatorcomment>1min(default)</translatorcomment>
        <translation type="obsolete">1min(default)</translation>
    </message>
    <message>
        <source>2分钟(默认)</source>
        <translatorcomment>2min(default)</translatorcomment>
        <translation type="obsolete">2min(default)</translation>
    </message>
    <message>
        <source>3分钟(默认)</source>
        <translatorcomment>3min(default)</translatorcomment>
        <translation type="obsolete">3min(default)</translation>
    </message>
    <message>
        <source>4分钟(默认)</source>
        <translatorcomment>4min(default)</translatorcomment>
        <translation type="obsolete">4min(default)</translation>
    </message>
    <message>
        <source>6分钟(默认)</source>
        <translatorcomment>6min(default)</translatorcomment>
        <translation type="obsolete">6min(default)</translation>
    </message>
    <message>
        <source>周一周二周三周四周五</source>
        <translation type="obsolete">Monday to Friday</translation>
    </message>
    <message>
        <source>24小时制(23:59:59)</source>
        <translatorcomment>24 hour system </translatorcomment>
        <translation type="obsolete">24 hour system</translation>
    </message>
    <message>
        <source>通知栏弹窗</source>
        <translatorcomment>Notification</translatorcomment>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>一分钟后自动关闭</source>
        <translatorcomment>Turn off after 1 min</translatorcomment>
        <translation type="obsolete">Turn off after 1 min</translation>
    </message>
</context>
<context>
    <name>Natice_alarm</name>
    <message>
        <location filename="../noticeAlarm.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="120"/>
        <source>Alarm clock</source>
        <translation>قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="227"/>
        <source>11:20 设计例会...</source>
        <translation>جىيىندىسىنا قاتىشۇۇسۇ پىلاندوو 11:20</translation>
    </message>
    <message>
        <source>60秒后自动关闭</source>
        <translation type="vanished">360 Seconds to close {60秒?}</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="334"/>
        <location filename="../noticeAlarm.cpp" line="140"/>
        <source>Remind later</source>
        <translation>سەل تۇرۇپ ەسكەرتۉۉ</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">响铃提示</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="112"/>
        <source>none</source>
        <translation>جوق</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="119"/>
        <source>Time out</source>
        <translation>ۇباقىت توشتۇ</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="146"/>
        <location filename="../noticeAlarm.cpp" line="149"/>
        <source>Alarm</source>
        <translation>قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="121"/>
        <location filename="../noticeAlarm.cpp" line="315"/>
        <source> Seconds to close</source>
        <translation> سەل  تۇرۇپ اپتوماتتىك   جابۇۇ</translation>
    </message>
</context>
<context>
    <name>Notice_Dialog</name>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">响铃提示</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <source>End of countdown time</source>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>秒后关闭铃声</source>
        <translation type="vanished">秒后关闭铃声</translation>
    </message>
    <message>
        <source>闹钟:</source>
        <translation type="vanished">闹钟:</translation>
    </message>
    <message>
        <source>起床铃</source>
        <translation type="vanished">起床铃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../clock.cpp" line="3089"/>
        <source>Hint</source>
        <translation>ەسكەرتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3090"/>
        <source>Are you sure to delete？</source>
        <translation>ازىرقى قوڭعۇراقتۇۇ سائەتنى ۅچۉرۉۉنۉ ايقىنداشتىراسىزبى؟</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3091"/>
        <source>sure</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3092"/>
        <source>cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
</context>
<context>
    <name>SelectBtnUtil</name>
    <message>
        <source>relax</source>
        <translation type="vanished">放松</translation>
    </message>
    <message>
        <source>emotion</source>
        <translation type="vanished">情感</translation>
    </message>
    <message>
        <source>silence</source>
        <translation type="vanished">静谧</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="211"/>
        <source>glass</source>
        <translation>اينەك</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="212"/>
        <source>bark</source>
        <translation>قاۋاش</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="213"/>
        <source>sonar</source>
        <translation>سونار</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="214"/>
        <source>drip</source>
        <translation>يامغۇر  تامچىسى</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="182"/>
        <source>diy bell</source>
        <translation>diy قوڭعۇراق</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="210"/>
        <source>none</source>
        <translation>جوق</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="313"/>
        <source>select bell</source>
        <translation>قوڭعۇراق وبونۇ تانداش</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="311"/>
        <source>audio files(*mp3 *wav *ogg)</source>
        <translation>(*mp3 *wav *ogg) تۉرلۅر</translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <location filename="../countdownAnimation.cpp" line="109"/>
        <source>TestWidget</source>
        <translation>TestWidget</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../utils.cpp" line="190"/>
        <source>none</source>
        <translation>جوق</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_24</name>
    <message>
        <source>PM</source>
        <translation type="vanished">下午</translation>
    </message>
    <message>
        <source>AM</source>
        <translation type="vanished">上午</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="244"/>
        <source>VerticalScroll_24</source>
        <translation>VerticalScroll_24</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalScroll60.cpp" line="176"/>
        <source>VerticalScroll_60</source>
        <translation>تىك  تومولونۇش  _ 60</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_99</name>
    <message>
        <location filename="../verticalScroll99.cpp" line="187"/>
        <source>VerticalScroll_99</source>
        <translation>VerticalScroll_99</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_APM</name>
    <message>
        <location filename="../verticalscrollapm.cpp" line="189"/>
        <source>VerticalScroll_APM</source>
        <translation>VerticalScroll_APM</translation>
    </message>
</context>
<context>
    <name>close_or_hide</name>
    <message>
        <location filename="../closeOrHide.ui" line="26"/>
        <location filename="../closeOrHide.cpp" line="129"/>
        <source>Dialog</source>
        <translation>دىيالوگ كەرەنەسى</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="322"/>
        <location filename="../closeOrHide.cpp" line="79"/>
        <location filename="../closeOrHide.cpp" line="87"/>
        <source>sure</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="418"/>
        <source>请选择关闭后的状态</source>
        <translation>ئېتىلگەندىن كىيىنكى  ابالىن تانداڭ</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="282"/>
        <location filename="../closeOrHide.cpp" line="91"/>
        <location filename="../closeOrHide.cpp" line="99"/>
        <source>cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="138"/>
        <location filename="../closeOrHide.cpp" line="168"/>
        <location filename="../closeOrHide.cpp" line="170"/>
        <source> backstage</source>
        <translation> ارقا سۇپادا يۈرۈشۈش</translation>
    </message>
    <message>
        <source>backstage</source>
        <translation type="vanished">后台运行</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="190"/>
        <source> Exit program </source>
        <translation> تىكەلەي ، تۉز ەلە جانىش ، قايتىش </translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="47"/>
        <source>Please select the state after closing:</source>
        <translation>:ئېتىلگەندىن كىيىنكى  ابالىن تانداڭ</translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="64"/>
        <source>Close</source>
        <translation>بەكىتىش</translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="187"/>
        <location filename="../closeOrHide.cpp" line="189"/>
        <source> Exit program</source>
        <translation> سىندىرۇۇ   پراگرامماسى</translation>
    </message>
</context>
<context>
    <name>delete_msg</name>
    <message>
        <location filename="../deleteMsg.ui" line="14"/>
        <source>Dialog</source>
        <translation>دىيالوگ كەرەنەسى</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="241"/>
        <source>sure</source>
        <translation>بەكىتۉۉ</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="206"/>
        <source>cancel</source>
        <translation>ارعادان  قالتىرىش</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="85"/>
        <source>are you sure ?</source>
        <translation>ازىرقى قوڭعۇراقتۇۇ سائەتنى چىقىرىۋېتىشنى ايقىنداشتىراسىزبى؟</translation>
    </message>
</context>
<context>
    <name>item_new</name>
    <message>
        <location filename="../itemNew.cpp" line="86"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>set_alarm_repeat_Dialog</name>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="39"/>
        <source>Alarm</source>
        <translation>قوڭعۇراقتۇۇ ساات</translation>
    </message>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="184"/>
        <source>Dialog</source>
        <translation>دىيالوگ كەرەنەسى</translation>
    </message>
</context>
<context>
    <name>setuppage</name>
    <message>
        <source>开机启动</source>
        <translatorcomment> Boot up</translatorcomment>
        <translation type="vanished"> Boot up</translation>
    </message>
    <message>
        <source>Boot up</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>  work</source>
        <translation type="vanished">  工作日</translation>
    </message>
    <message>
        <source>  Time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>  Pop-up</source>
        <translation type="vanished">  弹窗方式</translation>
    </message>
    <message>
        <source>  duration</source>
        <translation type="vanished">  稍后提醒</translation>
    </message>
    <message>
        <source>  ringtone</source>
        <translation type="vanished">  默认铃声</translation>
    </message>
    <message>
        <source>  Mute</source>
        <translation type="vanished">  静音</translation>
    </message>
    <message>
        <source>work</source>
        <translation type="vanished">工作日</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时间格式</translation>
    </message>
    <message>
        <source>Pop-up</source>
        <translation type="vanished">弹窗方式</translation>
    </message>
    <message>
        <source>duration</source>
        <translation type="vanished">稍后提醒</translation>
    </message>
    <message>
        <source>ringtone</source>
        <translation type="vanished">默认铃声</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">铃声音量</translation>
    </message>
    <message>
        <source>setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="vanished">周一</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="vanished">周二</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="vanished">周三</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="vanished">周四</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="vanished">周五</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="vanished">周六</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="vanished">周日</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation type="vanished">每天</translation>
    </message>
    <message>
        <source>Following system</source>
        <translation type="vanished">跟随系统</translation>
    </message>
    <message>
        <source>  time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>12 hour system</source>
        <translation type="vanished">12小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="vanished">全屏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 5 minutes</source>
        <translation type="vanished">5分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 10 minutes</source>
        <translation type="vanished">10分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 30 minutes</source>
        <translation type="vanished">30分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 60 minutes</source>
        <translation type="vanished">60分钟后提醒</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
</context>
<context>
    <name>stopwatch_item</name>
    <message>
        <source>longest</source>
        <translation type="vanished">最长</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="72"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="47"/>
        <location filename="../stopwatchItem.cpp" line="116"/>
        <source>max</source>
        <translation>ماكس</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="128"/>
        <source>min</source>
        <translation>ەڭ قىسقا</translation>
    </message>
    <message>
        <source>shortest</source>
        <translation type="vanished">最短</translation>
    </message>
</context>
<context>
    <name>tinyCountdown</name>
    <message>
        <source>Countdown</source>
        <translation type="vanished">倒计时</translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="26"/>
        <location filename="../tinycountdown.cpp" line="126"/>
        <source>tinyCountdown</source>
        <translation>tinyCountdown</translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="78"/>
        <source>01:29:58</source>
        <translation></translation>
    </message>
    <message>
        <source>switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="325"/>
        <source>close</source>
        <translation>جابۇۇ</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="333"/>
        <source>main window</source>
        <translation>نەگىزگى  كۅزۅنۅك</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="345"/>
        <source>suspend</source>
        <translation>ۇباقتىلۇۇ توقتوتۇۇ</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="364"/>
        <source>finish</source>
        <translation>اياقتاشتىرىش</translation>
    </message>
</context>
</TS>
