<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="87"/>
        <location filename="../about.ui" line="162"/>
        <source>Alarm</source>
        <translation>Saat</translation>
    </message>
    <message>
        <source>Kylin Alarm</source>
        <translation type="vanished">kylin saat</translation>
    </message>
    <message>
        <source>Clock</source>
        <translation type="vanished">kapat</translation>
    </message>
    <message>
        <source>Kylin Clock</source>
        <translation type="vanished">kylin saat</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>About</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Version: </source>
        <translation></translation>
    </message>
    <message>
        <source>Version: 2020.1.0</source>
        <translation type="vanished">Versyon: 2020.1.8</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="121"/>
        <location filename="../about.cpp" line="129"/>
        <source>Service &amp; Support: </source>
        <translation></translation>
    </message>
    <message>
        <source>Support and service team: support@kylinos.cn</source>
        <translation type="vanished">Destek ve hizmet ekibi:support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>BaseVerticalScroll</name>
    <message>
        <location filename="../baseverticalscroll.cpp" line="103"/>
        <source>AM</source>
        <translation type="unfinished">ÖÖ</translation>
    </message>
    <message>
        <location filename="../baseverticalscroll.cpp" line="105"/>
        <source>PM</source>
        <translation type="unfinished">ÖS</translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock</source>
        <translation type="vanished">Saat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="491"/>
        <location filename="../clock.cpp" line="498"/>
        <location filename="../clock.cpp" line="520"/>
        <location filename="../clock.cpp" line="1509"/>
        <location filename="../clock.cpp" line="1615"/>
        <location filename="../clock.cpp" line="3570"/>
        <source>start</source>
        <translation>Başlat</translation>
    </message>
    <message>
        <source>5min</source>
        <translation type="vanished">5 dk</translation>
    </message>
    <message>
        <source>15min</source>
        <translation type="vanished">15 dk</translation>
    </message>
    <message>
        <source>25min</source>
        <translation type="vanished">25 dk</translation>
    </message>
    <message>
        <source>30min</source>
        <translation type="vanished">30 dk</translation>
    </message>
    <message>
        <source>60min</source>
        <translation type="vanished">60 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="499"/>
        <location filename="../clock.cpp" line="1348"/>
        <location filename="../clock.cpp" line="3575"/>
        <location filename="../clock.cpp" line="3835"/>
        <source>suspend</source>
        <translation>Askıya Al</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="506"/>
        <source>add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="512"/>
        <source>no alarm</source>
        <translation>no Alarm</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">Sil</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="527"/>
        <source>save</source>
        <translation>Cts</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">Hatırlat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2453"/>
        <location filename="../clock.cpp" line="3706"/>
        <location filename="../clock.cpp" line="3809"/>
        <source>PM</source>
        <translation>ÖS</translation>
    </message>
    <message>
        <source>add alarm</source>
        <translation type="vanished">Alarm Ekle</translation>
    </message>
    <message>
        <source>Remaining time</source>
        <translation type="vanished">Kalan süre</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1393"/>
        <source>reset</source>
        <translation>Sıfırla</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="519"/>
        <location filename="../clock.cpp" line="1370"/>
        <location filename="../clock.cpp" line="1542"/>
        <source>count</source>
        <translation>İşaret</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="200"/>
        <location filename="../clock.cpp" line="3396"/>
        <location filename="../clock.cpp" line="3478"/>
        <source>Count down</source>
        <translation>Geri Sayım</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="14"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="vanished">Kronometre</translation>
    </message>
    <message>
        <source>deletealarm</source>
        <translation type="vanished">Alarmı Sil</translation>
    </message>
    <message>
        <source>Preservation</source>
        <translation type="vanished">Kaydet</translation>
    </message>
    <message>
        <source>12hour43minThe bell rings</source>
        <translation type="vanished">12 saat 43 dk zil çalıyor</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="523"/>
        <location filename="../clock.cpp" line="525"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>New alarm</source>
        <translation type="vanished">Yeni Alarm</translation>
    </message>
    <message>
        <source>  Name</source>
        <translation type="vanished">  İsim</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2159"/>
        <source>  repeat</source>
        <translation>  Tekrarla</translation>
    </message>
    <message>
        <source>  Remind</source>
        <translation type="vanished">  Hatırlat</translation>
    </message>
    <message>
        <source>  ring time</source>
        <translation type="vanished">  Çalma Zamanı</translation>
    </message>
    <message>
        <source> ring time</source>
        <translation type="vanished"> Çalma Zamanı</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="52"/>
        <source>On</source>
        <translation>Açık</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Devam Et</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1543"/>
        <source>interval </source>
        <translation>Aralık </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="528"/>
        <location filename="../clock.cpp" line="2460"/>
        <location filename="../clock.cpp" line="3708"/>
        <location filename="../clock.cpp" line="3816"/>
        <source>AM</source>
        <translation>ÖÖ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2318"/>
        <source>2min</source>
        <translation>2 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2165"/>
        <source>  remind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2320"/>
        <source>3min</source>
        <translation>3 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2322"/>
        <source>4min</source>
        <translation>4 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2324"/>
        <source>6min</source>
        <translation>6 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2331"/>
        <location filename="../clock.cpp" line="2522"/>
        <location filename="../clock.cpp" line="2899"/>
        <location filename="../clock.cpp" line="3007"/>
        <location filename="../clock.cpp" line="3008"/>
        <location filename="../clock.cpp" line="3235"/>
        <location filename="../clock.cpp" line="4262"/>
        <location filename="../clock.cpp" line="4263"/>
        <source>No repetition</source>
        <translation>Tekrar yok</translation>
    </message>
    <message>
        <source> Seconds to close</source>
        <translation type="vanished"> saniye sonra kapanacak</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2520"/>
        <location filename="../clock.cpp" line="2592"/>
        <location filename="../clock.cpp" line="3236"/>
        <location filename="../clock.cpp" line="4276"/>
        <location filename="../clock.cpp" line="4277"/>
        <source>Workingday</source>
        <translation>İş günü</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2605"/>
        <source>(default)</source>
        <translation>(varsayılan)</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2677"/>
        <location filename="../clock.cpp" line="2723"/>
        <source>Please set alarm name!</source>
        <translation>Lütfen alarm adını ayarlayın!</translation>
    </message>
    <message>
        <source>hour </source>
        <translation type="vanished">Saat </translation>
    </message>
    <message>
        <source> min bell rings</source>
        <translation type="vanished"> dk sonra alarm çalar</translation>
    </message>
    <message>
        <source>Edit alarm clock</source>
        <translation type="vanished">Alarm saatini düzenle</translation>
    </message>
    <message>
        <source>点击闹钟显示剩余时间</source>
        <translation type="vanished">Alarm saatini düzenle</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">Geri Sayım</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="202"/>
        <source>Watch</source>
        <translation>Kronometre</translation>
    </message>
    <message>
        <source> days </source>
        <translation type="vanished"> gün </translation>
    </message>
    <message>
        <source> hour </source>
        <translation type="vanished"> Saat </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2896"/>
        <source>glass</source>
        <translation>Bardak</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="890"/>
        <location filename="../clock.cpp" line="893"/>
        <location filename="../clock.cpp" line="907"/>
        <location filename="../clock.cpp" line="908"/>
        <location filename="../clock.cpp" line="2162"/>
        <source>  bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2025"/>
        <source>Minimize</source>
        <translation>küçültmek</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="565"/>
        <location filename="../clock.cpp" line="2061"/>
        <source>Quit</source>
        <translation>Çık</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menü</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2059"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2156"/>
        <source>  name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1055"/>
        <source>edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1568"/>
        <source>up to 100 times</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2011"/>
        <source>mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1670"/>
        <source>All bells are off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>360 Seconds to close</source>
        <translation type="vanished">Kapatmak için 360 Saniye</translation>
    </message>
    <message>
        <source>Time out</source>
        <translation type="vanished">Zaman doldu</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3539"/>
        <source>End</source>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3691"/>
        <source>after tomorrow</source>
        <translation>Yarından sonra</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3694"/>
        <source>Tomorrow</source>
        <translation>Yarın</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4203"/>
        <source>hour</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4206"/>
        <source>min</source>
        <translation>Dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3912"/>
        <source>sec</source>
        <translation>Sn</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2495"/>
        <location filename="../clock.cpp" line="2981"/>
        <location filename="../clock.cpp" line="3237"/>
        <location filename="../clock.cpp" line="4251"/>
        <source>Mon</source>
        <translation>Pzt</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1053"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1054"/>
        <source>ClearAll</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1381"/>
        <location filename="../clock.cpp" line="3855"/>
        <source>continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Up</source>
        <translation type="vanished">ayarlandır</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2060"/>
        <source>About</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2035"/>
        <source>Close</source>
        <translation>kapat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2047"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2069"/>
        <source>Version：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2351"/>
        <source>recent alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2497"/>
        <location filename="../clock.cpp" line="2983"/>
        <location filename="../clock.cpp" line="3238"/>
        <location filename="../clock.cpp" line="4252"/>
        <source>Tue</source>
        <translation>Sal</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2499"/>
        <location filename="../clock.cpp" line="2985"/>
        <location filename="../clock.cpp" line="3239"/>
        <location filename="../clock.cpp" line="4253"/>
        <source>Wed</source>
        <translation>Çar</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2501"/>
        <location filename="../clock.cpp" line="2987"/>
        <location filename="../clock.cpp" line="3240"/>
        <location filename="../clock.cpp" line="4254"/>
        <source>Thu</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2503"/>
        <location filename="../clock.cpp" line="2989"/>
        <location filename="../clock.cpp" line="3241"/>
        <location filename="../clock.cpp" line="4255"/>
        <source>Fri</source>
        <translation>Cum</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2505"/>
        <location filename="../clock.cpp" line="2991"/>
        <location filename="../clock.cpp" line="3242"/>
        <location filename="../clock.cpp" line="4256"/>
        <source>Sat</source>
        <translation>Cts</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2507"/>
        <location filename="../clock.cpp" line="2993"/>
        <location filename="../clock.cpp" line="3243"/>
        <location filename="../clock.cpp" line="4257"/>
        <source>Sun</source>
        <translation>Paz</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2517"/>
        <location filename="../clock.cpp" line="2518"/>
        <location filename="../clock.cpp" line="3002"/>
        <location filename="../clock.cpp" line="4399"/>
        <source>Every day</source>
        <translation>Her gün</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2614"/>
        <location filename="../clock.cpp" line="3764"/>
        <location filename="../clock.cpp" line="4462"/>
        <location filename="../clock.cpp" line="4466"/>
        <location filename="../clock.cpp" line="4484"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>the number of alarms reaches limit!！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2734"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3004"/>
        <location filename="../clock.cpp" line="3005"/>
        <source>  work</source>
        <translation>  İş</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3004"/>
        <source>  工作日</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3007"/>
        <source>不重复</source>
        <translation></translation>
    </message>
    <message>
        <source>60 Seconds to close</source>
        <translation type="obsolete">Kapatmak için 360 Saniye {60 ?}</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3765"/>
        <location filename="../clock.cpp" line="4469"/>
        <source>five mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3766"/>
        <location filename="../clock.cpp" line="4472"/>
        <source>ten mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3767"/>
        <location filename="../clock.cpp" line="4475"/>
        <source>twenty mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3768"/>
        <location filename="../clock.cpp" line="4478"/>
        <source>thirsty mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3769"/>
        <location filename="../clock.cpp" line="4481"/>
        <source>one hour late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="4679"/>
        <source>mini window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>glass(default)</source>
        <translation type="vanished">Bardak (varsayılan)</translation>
    </message>
    <message>
        <source>bark(default)</source>
        <translation type="vanished">Havlama (varsayılan)</translation>
    </message>
    <message>
        <source>sonar(default)</source>
        <translation type="vanished">Sonar(varsayılan)</translation>
    </message>
    <message>
        <source>drip(default)</source>
        <translation type="vanished">Damla(varsayılan)</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">2 dk</translation>
    </message>
    <message>
        <source>Monday to Friday</source>
        <translation type="vanished">Pazartesiden Cumaya</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24 saat düzeni</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2 dakika içinde uyar</translation>
    </message>
</context>
<context>
    <name>Natice_alarm</name>
    <message>
        <location filename="../noticeAlarm.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="120"/>
        <source>Alarm clock</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="227"/>
        <source>11:20 设计例会...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>工作会议</source>
        <translation type="vanished">Toplantı</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="334"/>
        <location filename="../noticeAlarm.cpp" line="140"/>
        <source>Remind later</source>
        <translation>Hatırlat</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">Kapat</translation>
    </message>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">Çalma İsteği</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="112"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="119"/>
        <source>Time out</source>
        <translation type="unfinished">Zaman doldu</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="146"/>
        <location filename="../noticeAlarm.cpp" line="149"/>
        <source>Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="121"/>
        <location filename="../noticeAlarm.cpp" line="315"/>
        <source> Seconds to close</source>
        <translation> dakika sonra kapanacak</translation>
    </message>
</context>
<context>
    <name>Notice_Dialog</name>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">Çalma İsteği</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <source>End of countdown time</source>
        <translation type="vanished">dk sonra kapanacak</translation>
    </message>
    <message>
        <source>秒后关闭铃声</source>
        <translation type="vanished">Zili saniyeler içinde kapatın</translation>
    </message>
    <message>
        <source>闹钟:</source>
        <translation type="vanished">Alarm saati:</translation>
    </message>
    <message>
        <source>起床铃</source>
        <translation type="vanished">Uyandırma zili</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../clock.cpp" line="3089"/>
        <source>Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3090"/>
        <source>Are you sure to delete？</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3091"/>
        <source>sure</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3092"/>
        <source>cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
</context>
<context>
    <name>SelectBtnUtil</name>
    <message>
        <location filename="../selectbtnutil.cpp" line="211"/>
        <source>glass</source>
        <translation type="unfinished">Bardak</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="212"/>
        <source>bark</source>
        <translation type="unfinished">Havlama</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="213"/>
        <source>sonar</source>
        <translation type="unfinished">Sonar</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="214"/>
        <source>drip</source>
        <translation type="unfinished">Damla</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="182"/>
        <source>diy bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="210"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="313"/>
        <source>select bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="311"/>
        <source>audio files(*mp3 *wav *ogg)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <location filename="../countdownAnimation.cpp" line="109"/>
        <source>TestWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../utils.cpp" line="190"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_24</name>
    <message>
        <source>PM</source>
        <translation type="vanished">ÖS</translation>
    </message>
    <message>
        <source>AM</source>
        <translation type="vanished">ÖÖ</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="244"/>
        <source>VerticalScroll_24</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalScroll60.cpp" line="176"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_99</name>
    <message>
        <location filename="../verticalScroll99.cpp" line="187"/>
        <source>VerticalScroll_99</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_APM</name>
    <message>
        <location filename="../verticalscrollapm.cpp" line="189"/>
        <source>VerticalScroll_APM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>close_or_hide</name>
    <message>
        <location filename="../closeOrHide.ui" line="26"/>
        <location filename="../closeOrHide.cpp" line="129"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="322"/>
        <location filename="../closeOrHide.cpp" line="79"/>
        <location filename="../closeOrHide.cpp" line="87"/>
        <source>sure</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="418"/>
        <source>请选择关闭后的状态</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="282"/>
        <location filename="../closeOrHide.cpp" line="91"/>
        <location filename="../closeOrHide.cpp" line="99"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="138"/>
        <location filename="../closeOrHide.cpp" line="168"/>
        <location filename="../closeOrHide.cpp" line="170"/>
        <source> backstage</source>
        <translation> Arka Çalış</translation>
    </message>
    <message>
        <source>backstage</source>
        <translation type="vanished">Arka Çalış</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="190"/>
        <source> Exit program </source>
        <translation> Kapat </translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="47"/>
        <source>Please select the state after closing:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="64"/>
        <source>Close</source>
        <translation type="unfinished">kapat</translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="187"/>
        <location filename="../closeOrHide.cpp" line="189"/>
        <source> Exit program</source>
        <translation> Kapat </translation>
    </message>
</context>
<context>
    <name>delete_msg</name>
    <message>
        <location filename="../deleteMsg.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="241"/>
        <source>sure</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="206"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="85"/>
        <source>are you sure ?</source>
        <translation>Emin misiniz?</translation>
    </message>
</context>
<context>
    <name>item_new</name>
    <message>
        <location filename="../itemNew.cpp" line="86"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>set_alarm_repeat_Dialog</name>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="39"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="184"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>setuppage</name>
    <message>
        <source>开机启动</source>
        <translation type="vanished">Başlat</translation>
    </message>
    <message>
        <source>Boot up</source>
        <translation type="vanished">Önyükleme</translation>
    </message>
    <message>
        <source>  work</source>
        <translation type="vanished">  İş</translation>
    </message>
    <message>
        <source>  Time</source>
        <translation type="vanished">  Zaman</translation>
    </message>
    <message>
        <source>  Pop-up</source>
        <translation type="vanished">  Açılır</translation>
    </message>
    <message>
        <source>  duration</source>
        <translation type="vanished">  Süre</translation>
    </message>
    <message>
        <source>  ringtone</source>
        <translation type="vanished">  Zil sesi</translation>
    </message>
    <message>
        <source>  Mute</source>
        <translation type="vanished">  Sessiz</translation>
    </message>
    <message>
        <source>work</source>
        <translation type="vanished">İş</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Zaman</translation>
    </message>
    <message>
        <source>Pop-up</source>
        <translation type="vanished">Açılır</translation>
    </message>
    <message>
        <source>duration</source>
        <translation type="vanished">Süre</translation>
    </message>
    <message>
        <source>ringtone</source>
        <translation type="vanished">Zil sesi</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">Sessiz</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">Ayar</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="vanished">Pzt</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="vanished">Sal</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="vanished">Çar</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="vanished">Per</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="vanished">Cum</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="vanished">Cts</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="vanished">Paz</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation type="vanished">Her Gün</translation>
    </message>
    <message>
        <source>Following system</source>
        <translation type="vanished">Takip sistemi</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24 saat düzeni</translation>
    </message>
    <message>
        <source>12 hour system</source>
        <translation type="vanished">12 saat düzeni</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="vanished">Tam ekran</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 5 minutes</source>
        <translation type="vanished">5 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 10 minutes</source>
        <translation type="vanished">10 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 30 minutes</source>
        <translation type="vanished">30 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 60 minutes</source>
        <translation type="vanished">60 dakika içinde uyar</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
</context>
<context>
    <name>stopwatch_item</name>
    <message>
        <location filename="../stopwatchItem.cpp" line="72"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="47"/>
        <location filename="../stopwatchItem.cpp" line="116"/>
        <source>max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="128"/>
        <source>min</source>
        <translation type="unfinished">Dk</translation>
    </message>
</context>
<context>
    <name>tinyCountdown</name>
    <message>
        <location filename="../tinycountdown.ui" line="26"/>
        <location filename="../tinycountdown.cpp" line="126"/>
        <source>tinyCountdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="78"/>
        <source>01:29:58</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="325"/>
        <source>close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="333"/>
        <source>main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="345"/>
        <source>suspend</source>
        <translation type="unfinished">Askıya Al</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="364"/>
        <source>finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
