/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CLOSE_OR_HIDE_H
#define CLOSE_OR_HIDE_H

#include <QDialog>
#include <QStyleOption>
#include <QPainter>
#include <QPainterPath>
#include <QMouseEvent>
#include <QGSettings/qgsettings.h>
#include <QBoxLayout>
#include <QLabel>
#include <QRadioButton>

namespace Ui {
class close_or_hide;
}
class Clock;
class close_or_hide : public QDialog
{
    Q_OBJECT

public:
    explicit close_or_hide(QWidget *parent = nullptr);
    ~close_or_hide();
    void setupUi(QDialog *close_or_hide);
    void retranslateUi(QDialog *close_or_hide);
    void iniframe();
    QWidget *widget;
    QWidget *verticalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *closebtn;
    QWidget *verticalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QRadioButton *backrunRadio;
    QSpacerItem *horizontalSpacer_3;
    QRadioButton *exitRadio;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *cancelbtn;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *surebtn;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QLabel *questionIconLabel;
    QLabel *closeInfoLabel;

    void updatePosition(bool onTablet);

    //绘制底部阴影
    // Draw bottom shadow
    void paintEvent(QPaintEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    int close_flag;
private slots:
    void on_closebtn_clicked();

    void on_surebtn_clicked();

    void on_cancelbtn_clicked();

    void  settingsStyle();                                                               // 监听主题

private:
    Ui::close_or_hide *ui;
    QPoint dragPosition;                                            //拖动坐标
    bool mousePressed;                                              //鼠标是否按下
};

#endif // CLOSE_OR_HIDE_H
