/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "commontooltip.h"
#include <QBitmap>
#include <QPainter>
#include "theme.h"
#include <QVBoxLayout>

CommonToolTip::CommonToolTip(QWidget *parent) : kdk::KBallonTip(parent)
{

    this->setAttribute(Qt::WA_TranslucentBackground);//设置窗口背景透明
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::ToolTip); //设置无边框窗口
    this->setTipType(kdk::TipType::Nothing);

}

void CommonToolTip::showThisToolTip()
{
    this->setTipTime(2000);
    this->showInfo();
}
