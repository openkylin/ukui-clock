/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "CustomButton.h"
#include <QDebug>
#include "theme.h"

CustomButton::CustomButton(QWidget *parent,int width, int height, int status) : QPushButton(parent),m_width(width),m_height(height),position(0),Status(status)
{
    max = qMax(m_width,m_height);
    min = m_width>m_height?m_height:m_width;
    length = max - min;
    init(status);
}

CustomButton::~CustomButton()
{
}

//开
void CustomButton::openSlot()
{
    //滑动动画
    animation1 = new QPropertyAnimation(myLabel, "geometry");
    animation1->setDuration(100);
    animation1->setKeyValueAt(0, QRect(2, 2, 20, 20));
    animation1->setEndValue(QRect(31, 2, 20, 20));
    animation1->start();
    this->setStyleSheet(openBcack);
    myLabel->setStyleSheet(openbtn);
    Status = 1;
}

//关
void CustomButton::closeSlot()
{
    animation1 = new QPropertyAnimation(myLabel, "geometry");
    animation1->setDuration(100);
    animation1->setKeyValueAt(0, QRect(31, 2, 20, 20));
    animation1->setEndValue(QRect(2, 2, 20, 20));
    animation1->start();
    this->setStyleSheet(closeBcack);
    myLabel->setStyleSheet(closeBtn);
    Status = 0;
}

void CustomButton::colorUpdate()
{
    switch(Status)
    {
    case 0:
            this->setStyleSheet(closeBcack);
            myLabel->setStyleSheet(closeBtn);
        break;

    case 1:
            this->setStyleSheet(openBcack );
            myLabel->setStyleSheet(openbtn);
        break;
    }
}

void CustomButton::init(int status)
{
    this->resize(m_width,m_height);
    myLabel = new QLabel(this);
    myLabel->setObjectName("myLabel");
    myLabel->resize(20,20);
    this->setStyleSheet(openBcack);
    myLabel->setStyleSheet(openbtn);
    this->setFixedSize(m_width,m_height);
    settingsStyle();
}

void CustomButton::initClose()
{
    this->setStyleSheet(closeBcack);
    myLabel->setStyleSheet(closeBtn);
    Status = 0;
    myLabel->move(2,2);
}

void CustomButton::initOpen()
{
    Status = 1;
    myLabel->move(31,2);
    this->setStyleSheet(openBcack);
    myLabel->setStyleSheet(openbtn);
}

/*
*监听主题
*/
void CustomButton::settingsStyle()
{
    GsettingSubject * subject = GsettingSubject::getInstance();
    connect(subject,&GsettingSubject::blackStyle, this,[=](){
        this->blackStyle();
    });
    connect(subject,&GsettingSubject::whiteStyle, this,[=](){
        this->whiteStyle();
    });
    subject->iniWidgetStyle();
}

QColor BACK_COLOR = QColor(220, 220, 220, 255);
//黑色主题
void  CustomButton::blackStyle()
{

    closeBcack = QString("CustomButton{background-color:rgba(240, 247, 255, 0.2);border-radius:%1px;}").arg(min/2);
    QColor b1 = QColor(70, 70, 70, 255);
    QString btnColor=theme::getColorStr(b1);
    QColor btnHover=QColor(100, 100, 100, 255);
    QString btnHoverColor=theme::getColorStr(btnHover);
    closeBcack = QString("CustomButton{background-color:"+btnColor+";border-radius:%1px;}CustomButton:pressed{background-color:"+btnHoverColor+";}CustomButton:hover{background-color:"+btnHoverColor+";}").arg(min/2);
    closeBtn = QString("#myLabel{background-color:rgba(235, 244, 255, 0.55);border-radius:%1px}").arg(20/2);

    QColor deepHigh=theme::highLight_Hover();
    QString deepHiColor=theme::getColorStr(deepHigh);
    QColor h1 = this->palette().color(QPalette::Inactive,QPalette::Highlight);
    QString hiColor=theme::getColorStr(h1);
    openBcack = QString("CustomButton{background-color:"+hiColor+";border-radius:%1px;}CustomButton:pressed{background-color:"+deepHiColor+";}CustomButton:hover{background-color:"+deepHiColor+";}").arg(min/2);
    openbtn = QString("#myLabel{background-color:rgba(255, 255, 255, 0.88);border-radius:%1px}").arg(20/2);
    colorUpdate();

}


//白色主题
void  CustomButton::whiteStyle()
{
    closeBcack = QString("CustomButton{background-color:rgba(180, 195, 212, 1);border-radius:%1px;}").arg(min/2);
    QString btnColor=theme::getColorStr(BACK_COLOR);
    QColor btnClick=theme::button_Click();
    QString btnClickColor=theme::getColorStr(btnClick);
    closeBcack = QString("CustomButton{background-color:"+btnColor+";border-radius:%1px;}CustomButton:pressed{background-color:"+btnClickColor+";}CustomButton:hover{background-color:"+btnClickColor+";}").arg(min/2);
    closeBtn = QString("#myLabel{background-color: rgb(240, 245, 250);border-radius:%1px}").arg(20/2);

    QColor deepHigh=theme::highLight_Click();
    QString deepHiColor=theme::getColorStr(deepHigh);
    QColor h1 = this->palette().color(QPalette::Inactive,QPalette::Highlight);
    QString hiColor=theme::getColorStr(h1);
    openBcack = QString("CustomButton{background-color:"+hiColor+";border-radius:%1px;}CustomButton:pressed{background-color:"+deepHiColor+";}CustomButton:hover{background-color:"+deepHiColor+";}").arg(min/2);
    openbtn = QString("#myLabel{background-color: rgba(255, 255, 255, 0.88);border-radius:%1px}").arg(20/2);
    colorUpdate();
}
