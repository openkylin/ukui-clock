/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "mediaplayerpool.h"
#include <QDateTime>
#include <QApplication>
MediaPlayerPool::MediaPlayerPool(int size,QObject *parent) : QObject(parent),m_size(size)
{
    workerThread = new MediaPlayerThread(size,this);
    createQMediaPlayerPool(m_size);
}

MediaPlayerThread *MediaPlayerPool::getWorkerThread() const
{
    return workerThread;
}

/**
 * @brief 创建对象池
 */
void MediaPlayerPool::createQMediaPlayerPool(int size)
{
    connect(workerThread, &MediaPlayerThread::resultReady, this, [=](ObjectPool<QMediaPlayer> * mediaPool){
        m_qMediaPlayerPool=mediaPool;
    });
    connect(workerThread, &MediaPlayerThread::finished, workerThread, &QObject::deleteLater);
    workerThread->start();
    /*
    qDebug()<<"dbq-开始创建对象池"<<m_size;
    qint64 timeT1 = QDateTime::currentDateTime().toMSecsSinceEpoch();
    ObjectPool<QMediaPlayer> * que = new ObjectPool<QMediaPlayer>(m_size);
    m_qMediaPlayerPool=que;
    qint64 timeT2 = QDateTime::currentDateTime().toMSecsSinceEpoch();
    qDebug()<<"dbq-创建耗时"<<timeT2-timeT1;
    */
}


/**
 * @brief 取播放器
 * @return
 */
QMediaPlayer *MediaPlayerPool::getQMediaPlayer()
{
    QMediaPlayer * res = nullptr;
    if(m_qMediaPlayerPool!=nullptr){
        qDebug()<<"dbq-从池中获取";
        res=m_qMediaPlayerPool->GetObject();
        //清空数据
        res->setPlaylist(NULL);
        res->setMedia(NULL);
//        res->stop();
//        res->moveToThread(QApplication::instance()->thread());
        qDebug()<<"dbq-state"<<res->state()<<"MediaStatus"<<res->mediaStatus();
    }else{
        qDebug()<<"dbq-新建";
        qint64 timeT1 = QDateTime::currentDateTime().toMSecsSinceEpoch();
        res = new QMediaPlayer();
        qint64 timeT2 = QDateTime::currentDateTime().toMSecsSinceEpoch();
        qDebug()<<"dbq-创建耗时"<<timeT2-timeT1;
    }
    return res;
}

void MediaPlayerPool::pushBackQMediaPlayer(QMediaPlayer *player)
{
    if(m_qMediaPlayerPool!=nullptr){
        m_qMediaPlayerPool->returnObject(player);
    }
}

MediaPlayerPool::~MediaPlayerPool()
{
    delete m_qMediaPlayerPool;
}
