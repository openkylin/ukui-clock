/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ROUNDBTN_H
#define ROUNDBTN_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QBrush>

class RoundBtn : public QPushButton
{
    Q_OBJECT
public:
    explicit RoundBtn(QWidget *parent = 0);    //explicit 避免歧义
//    RoundBtn(const QString text, QPoint center,int radius,QWidget* parent = 0);
    RoundBtn(const QString text, int radius,QWidget* parent = 0);
    enum BTN_TYPE{
        TEXT_FLAG = 0,
        ICON_FLAG = 1,
    };
    RoundBtn(const QString text, int radius,RoundBtn::BTN_TYPE flag = TEXT_FLAG,QWidget* parent = 0);
    RoundBtn(RoundBtn::BTN_TYPE flag = TEXT_FLAG,QWidget* parent = 0);

    void paintEvent(QPaintEvent *) override;                //--绘图事件，调用update()时触发
    void mousePressEvent(QMouseEvent *e) override;         //--鼠标按下事件
    void mouseReleaseEvent(QMouseEvent *e) override;     //--鼠标释放事件
    void mouseMoveEvent(QMouseEvent *e) override;       //--鼠标移动事件
    void enterEvent(QEvent *event) override;
    void leaveEvent(QEvent *event) override;

    bool isContains(QPoint p);//判断鼠标是否在圆形范围之内
    void initForm();//初始化窗体
    void setText(const QString &text);
    void setBtnColor (QColor defaultColor,QColor hoverColor,QColor clickColor);
    void setPenColor (QColor defaultColor,QColor hoverColor,QColor clickColor);
    void updateTextPenColor(QColor textPenColor);
public slots:
private:
    QString m_text;    //控件显示文本
//    QPoint _center;  //圆心位置坐标
    QPoint beginPos;//圆形起始坐标
    int m_radius;   //圆形半径
    QPixmap  getIconPixmap(QString filePath);
    QString m_defaultIconPath;
    QString m_hoverIconPath;
    QString m_pressIconPath;
    QColor m_pressColor;
    QColor m_hoverColor;
    QColor m_defaultColor;
    QSize m_IconSize;
    int m_angleRound = 6;

protected:
    QBrush pressBrush;
    QBrush notPressBrush;
    QBrush hoverBrush;
    QBrush notHoverBrush;
    QColor penColor;
    QColor penClickColor;
    QColor penHoverColor;
    BTN_TYPE m_flag;
public:
    bool _pressed;//左键单击变色控制
    bool _hover;    //悬浮高亮
    void setIconPath(const QString &iconPath);
    void setRadius(int radius);
    void setAngleRound(int angleRound);
    void setIconSize(const QSize &IconSize);
    void setAllIconPatn(const QString &defaultIconPath,const QString &hoverIconPath,const QString &pressIconPath);
    void setFlag(const BTN_TYPE &flag);
    void setBtnBackcolorTransparent();
    void setBtnHighlightColorTransparent();
};

#endif // ROUNDBTN_H
