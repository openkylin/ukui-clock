/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "dotlineDemo.h"
#include <QDebug>
#include <QStyleOption>
#include <QPainterPath>
#include "theme.h"

DotLineDemo::DotLineDemo(QWidget *parent):
     QWidget(parent)
{
    this->resize(390, 310);
}

DotLineDemo::~DotLineDemo()
{
}

//绘制虚线圈
// Draw a dashed circle
void DotLineDemo::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);

    QStyleOption opt;
    opt.init(this);
    QColor mainColor;
    mainColor = theme::countdownRingBackColor;
    painter.save();
    painter.setPen(mainColor);
    painter.setBrush(mainColor);
    QPainterPath bigCircle;
    bigCircle.addEllipse(65, 13, 266, 266);
    QPainterPath path = bigCircle ;
    painter.drawPath(path);
    painter.restore();
}
