/*
 *
 * Copyright (C) 2015 Toggl Desktop developers.
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 *
 * You may use this file under the terms of the BSD license as follows:
 *
 * "Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
 *     of its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 */

#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <QObject>
#include <QApplication>
#include <QtNetwork/QLocalServer>
#include "clock.h"

class SingleApplication : public QApplication {
    Q_OBJECT
public:
    SingleApplication(int &argc, char **argv);

    bool isRunning();              //是否已经有实例在运行
                                   //  Whether an instance is already running
    Clock *w;                      // Widget指针
                                   // Widget pointer

private slots:
    // 有新连接时触发
    // Triggered when there is a new connection
    void _newLocalConnection();

private:
    // 初始化本地连接
    // Initialize local connection
    void _initLocalConnection();
    // 创建服务端
    // Create server
    void _newLocalServer();
    // 激活窗口
    // Activate window
    void _activateWindow();

    bool _isRunning;                // 是否已經有实例在运行
                                    // Whether an instance is already running
    QLocalServer *_localServer;     // 本地socket Server
                                    // Local socket server
    QString _serverName;            // 服务名称
                                    // Service name
};


#endif // SINGLEAPPLICATION_H
