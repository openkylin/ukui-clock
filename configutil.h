/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGUTIL_H
#define CONFIGUTIL_H

#include <QObject>

#include <QVariant>
#include <QSettings>

class ConfigUtil : public QObject
{
    Q_OBJECT
public:
    explicit ConfigUtil(QString qstrfilename = "",QObject *parent = nullptr);
    void Set(QString,QString,QVariant);
    QVariant Get(QString,QString);


signals:
private:
    QString m_qstrFileName;
    QSettings *m_psetting;

};

#endif // CONFIGUTIL_H
