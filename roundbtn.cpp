/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "roundbtn.h"
#include <QPainter>
#include <QDebug>
#include "constant_class.h"

RoundBtn::RoundBtn(QWidget *parent):
    QPushButton(parent),m_text(""),m_radius(14),m_defaultIconPath(""),m_hoverIconPath(""),m_pressIconPath("")
  ,m_pressColor(Qt::gray),m_hoverColor(Qt::gray),m_defaultColor(Qt::gray),m_flag(TEXT_FLAG)
{
    initForm();
}
RoundBtn::RoundBtn(const QString text, int radius, QWidget *parent):
    QPushButton(parent),m_text(text),m_radius(radius),m_flag(TEXT_FLAG)
    {
    initForm();
}

RoundBtn::RoundBtn(RoundBtn::BTN_TYPE flag, QWidget *parent):
    QPushButton(parent),m_text(""),m_radius(28),m_flag(flag)
{
    initForm();
}

RoundBtn::RoundBtn(const QString text, int radius, RoundBtn::BTN_TYPE flag, QWidget *parent):
    QPushButton(parent),m_text(text),m_radius(radius),m_flag(flag)
{
    initForm();
}


void RoundBtn::paintEvent(QPaintEvent *)
{
    QPainter p(this);//将当前窗体作为画布
    p.save();
    p.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    QPen pen = QPen(penColor);
    /*
    if(m_flag==TEXT_FLAG){
        if(_pressed){
            p.setBrush(pressBrush);
            pen.setColor(penClickColor);
        }else{
            p.setBrush(notPressBrush);
            pen.setColor(penColor);
        }
        if(_hover){
            p.setBrush(hoverBrush);
            pen.setColor(penHoverColor);
        }else{
            p.setBrush(notHoverBrush);
            pen.setColor(penColor);
        }
        p.setPen(Qt::NoPen);//没有线条
    }else{
        p.setPen(Qt::NoPen);
        p.setBrush(Qt::NoBrush);
    }
    */
    if(_pressed){
        p.setBrush(pressBrush);
        pen.setColor(penClickColor);
    }else{
        p.setBrush(notPressBrush);
        pen.setColor(penColor);
    }
    if(_hover){
        p.setBrush(hoverBrush);
        pen.setColor(penHoverColor);
    }else{
        p.setBrush(notHoverBrush);
        pen.setColor(penColor);
    }
    p.setPen(Qt::NoPen);//没有线条



    //画圆形
//    QPoint _center = this->rect().center();
//    p.drawEllipse(_center,_radius,_radius);
    QRect rect = this->rect();
    rect.setWidth(rect.width() - 1);
    rect.setHeight(rect.height() - 1);
    //圆角
    p.drawRoundedRect(rect, m_radius, m_radius);
    p.restore();
    if(m_flag==TEXT_FLAG){
        p.save();
        //添加文本
        p.setPen(pen);
        p.drawText(rect, m_text, QTextOption(Qt::AlignCenter));//文本居中：QTextOption(Qt::AlignCenter)
        p.restore();
    }else if(m_flag==ICON_FLAG){
        QPixmap pixmp = getIconPixmap(m_defaultIconPath);
        if(_pressed){
            pixmp = getIconPixmap(m_pressIconPath);
        }
        if(_hover){
            pixmp = getIconPixmap(m_hoverIconPath);
        }
        p.save();
        p.drawPixmap(QRect(-1,-1,m_IconSize.width(),m_IconSize.height()),pixmp);
        p.restore();
    }

}

void RoundBtn::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e)
    _pressed = true;
}

void RoundBtn::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e)
   if(_pressed)
   {
       _pressed = false;
       emit clicked();
   }
}

void RoundBtn::mouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e)
}

void RoundBtn::enterEvent(QEvent *event)
{
    Q_UNUSED(event)
    _hover = true;
    update();
}

void RoundBtn::leaveEvent(QEvent *event)
{
    Q_UNUSED(event)
    _hover = false;
    update();
}

bool RoundBtn::isContains(QPoint p)
{
    Q_UNUSED(p)
    return true;
}

void RoundBtn::initForm()
{
    _pressed = false;
    _hover = false;
     pressBrush = QBrush(Qt::gray);
     notPressBrush = QBrush(QColor("lightGrey"));
     hoverBrush = QBrush(Qt::gray);
     notHoverBrush = QBrush(QColor("lightGrey"));
     penColor = QColor(Qt::black);
     penClickColor = QColor(Qt::black);
     penHoverColor = QColor(Qt::black);
//    beginPos = this->pos();
    //无边框
//    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint|Qt::WindowStaysOnTopHint);
    /*  窗口整体透明，但窗口控件不透明*/
//   this->setAttribute(Qt::WA_TranslucentBackground,true);


}

void RoundBtn::setText(const QString &text)
{
    m_text = text;
    update();
}

void RoundBtn::setBtnColor(QColor defaultColor, QColor hoverColor, QColor clickColor)
{
    pressBrush = QBrush(clickColor);
    notPressBrush = QBrush(defaultColor);
    hoverBrush = QBrush(hoverColor);
    notHoverBrush = QBrush(defaultColor);
}

void RoundBtn::setPenColor(QColor defaultColor, QColor hoverColor, QColor clickColor)
{
    penColor = defaultColor;
    penHoverColor = hoverColor;
    penClickColor = clickColor;
}
/**
 * @brief 修改文字颜色
 */
void RoundBtn::updateTextPenColor(QColor textPenColor)
{
    penColor=textPenColor;
    update();
}

void RoundBtn::setRadius(int radius)
{
    m_radius = radius;
}

QPixmap  RoundBtn::getIconPixmap(QString filePath)
{
    QPixmap pixmap;
    pixmap.load(filePath);
    return pixmap;
}

void RoundBtn::setFlag(const BTN_TYPE &flag)
{
    m_flag = flag;
}

void RoundBtn::setBtnBackcolorTransparent()
{
    QPalette pale =this->palette();
    QColor color = pale.color(QPalette::Button);
    QColor ColorPlaceholderText3 = QColor(255,255,255,0);
    QBrush brush;
    brush.setColor(ColorPlaceholderText3);
    pale.setBrush(QPalette::Button, brush);
    this->setPalette(pale);
}

void RoundBtn::setBtnHighlightColorTransparent()
{
    QPalette pale =this->palette();
    QColor color = pale.color(QPalette::Button);
    QColor ColorPlaceholderText3 = QColor(255,255,255,0);
    QBrush brush;
    brush.setColor(ColorPlaceholderText3);
    pale.setBrush(QPalette::Highlight, brush);
    this->setPalette(pale);
}



void RoundBtn::setAllIconPatn(const QString &defaultIconPath, const QString &hoverIconPath, const QString &pressIconPath)
{
    m_pressIconPath = pressIconPath;
    m_hoverIconPath = hoverIconPath;
    m_defaultIconPath = defaultIconPath;
    update();
}

void RoundBtn::setIconSize(const QSize &IconSize)
{
    m_IconSize = IconSize;
}

void RoundBtn::setAngleRound(int angleRound)
{
    m_angleRound = angleRound;
}

void RoundBtn::setIconPath(const QString &iconPath)
{
    m_defaultIconPath = iconPath;
}


