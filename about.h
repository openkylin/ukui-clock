/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QPainter>
#include <QPainterPath>
#include <QDesktopServices>
#include <QUrl>
#include <QGSettings>
#include "constant_class.h"

#include <QLabel>
#include <math.h>

#include "gsettingsubject.h"





namespace Ui {
class About;
}

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = nullptr);
    ~About();
    void showThisWindow();

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    void  settingsStyle();                                                               //监听主题
    void  blackStyle();                                                                  //黑色主题
    void  whiteStyle();                                                                  //白色主题
    void updateLabelFront(QLabel *label, int size);
    int CURRENT_FONT_SIZE;
    Ui::About *ui;
};

#endif // ABOUT_H
